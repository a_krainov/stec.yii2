
const rootViewBox = document.getElementById("svgRoot").viewBox.baseVal;

document
    .getElementById("value")
    .addEventListener("input", (ev) => {
      if (!ev.target.value) return;

      const el = document.getElementById("text");

      const before = el.getBoundingClientRect();
      let text = ev.target.value;

      var fontSize=14;
      var lineHeight=1;
      var tspans = text.split('\n').map(function(a,i){
        var yVal = fontSize*lineHeight+i*fontSize*lineHeight;
        var tspan = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
        tspan.setAttributeNS(null,"x",0);
        tspan.setAttributeNS(null,"y", yVal);
        tspan.textContent = a;
        return tspan;
      });

      el.innerHTML = '';
      tspans.forEach(function(elem) {
        el.appendChild(elem);
      });

      const after = el.getBoundingClientRect();

      rootViewBox.width = before.height && after.height ? rootViewBox.width * after.height / before.height : rootViewBox.width;
      rootViewBox.height = before.width && after.width ? rootViewBox.height * after.width / before.width : rootViewBox.height;
    });
