(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

var rootViewBox = document.getElementById("svgRoot").viewBox.baseVal;

document.getElementById("value").addEventListener("input", function (ev) {
      if (!ev.target.value) return;

      var el = document.getElementById("text");

      // const before = el.getBoundingClientRect();
      var text = ev.target.value;

      var fontSize = 14;
      var lineHeight = 1;
      var tspans = text.split('\n').map(function (a, i) {
            var yVal = fontSize * lineHeight + i * fontSize * lineHeight;
            var tspan = document.createElementNS("http://www.w3.org/2000/svg", "tspan");
            tspan.setAttributeNS(null, "x", 0);
            tspan.setAttributeNS(null, "y", yVal);
            tspan.textContent = a;
            return tspan;
      });

      el.innerHTML = '';
      tspans.forEach(function (elem) {
            el.appendChild(elem);
      });

      // const after = el.getBoundingClientRect();

      // console.log();
      var newSize = el.getBBox();

      rootViewBox.width = newSize.width;
      rootViewBox.height = newSize.height;

      // rootViewBox.width = before.height && after.height ? rootViewBox.width * after.height / before.height : rootViewBox.width;
      // rootViewBox.height = before.width && after.width ? rootViewBox.height * after.width / before.width : rootViewBox.height;
});

},{}]},{},[1]);
