<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "department_list".
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 *
 * @property UserListUniversity0[] $userListUniversity0s
 */
class DepartmentList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_ru', 'name_en'], 'required'],
            [['name_ru', 'name_en'], 'string', 'max' => 255],
            [['university_id'], 'integer'],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniversityList::className(), 'targetAttribute' => ['university_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_ru' => 'Название Рус',
            'name_en' => 'Название Анг.',
            'university_id' => 'Университет',
            'universityName' => 'Университет',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {
            
            if ($this->university_id  == '') {
                $this->university_id = Yii::$app->user->identity->university_id;
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserListUniversity0s()
    {
        return $this->hasMany(UserListUniversity0::className(), ['department_id' => 'id']);
    }
    public function getUniversity()
    {
        return $this->hasOne(UniversityList::className(), ['id' => 'university_id']);
    }

    /* Геттер для названия универа */
    public function getUniversityName() {
        return $this->university->name;
    }
}
