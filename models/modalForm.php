<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 11.05.2018
 * Time: 13:33
 */

namespace app\models;


class modalForm extends Model
{
    public $code;
    public $name;

    public $price_new;
    public $price_old;

    public $meta_title;
    public $meta_description;

    public function rules()
    {
        return [
            [['code', 'name', 'price_new'], 'required'],
            [['code', 'name', 'meta_title'], 'string', 'max' => 255],
            [['code'], 'unique', 'targetClass' => Brand::class],
            [['meta_description'], 'string'],
            [['price_new', 'price_old'], 'integer'],
        ];
    }
}