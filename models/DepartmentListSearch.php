<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DepartmentList;

/**
 * DepartmentListSearch represents the model behind the search form of `app\models\DepartmentList`.
 */
class DepartmentListSearch extends DepartmentList
{
    /* вычисляемый атрибут */
    public $universityName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name_ru', 'name_en'], 'safe'],
            [['universityName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DepartmentList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'universityName' => [
                    'asc' => ['university_list.name' => SORT_ASC],
                    'desc' => ['university_list.name' => SORT_DESC],
                    'label' => 'Вуз'
                ],
                'name_ru',
                'name_en',
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Страны
             * для работы сортировки.
             */

            //если юсер не суперадмин, то только свои ВУЗ
            $query->joinWith(['university' => function ($q) {
                if (Yii::$app->user->identity->role == User::ROLE_ADMIN) {
                    $q->where('university_list.id = "' . Yii::$app->user->identity->university_id . '"');
                }
            }]);

            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'department_list.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'department_list.name_ru', $this->name_ru])
            ->andFilterWhere(['like', 'department_list.name_en', $this->name_en]);

        // Фильтр по вузу
        if (Yii::$app->user->identity->role == User::ROLE_ADMIN){
            $query->joinWith(['university' => function ($q) {
                $q->where('university_list.name LIKE "%' . $this->universityName . '%"');
                $q->andWhere('university_list.id = "' . Yii::$app->user->identity->university_id. '"');
            }]);
        } else {
            $query->joinWith(['university' => function ($q) {
                $q->where('university_list.name LIKE "%' . $this->universityName . '%"');
            }]);
        }

        return $dataProvider;
    }
}
