<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maket_list".
 *
 * @property int $id
 * @property string $path
 *
 * @property UserListUniversity1[] $userListUniversity1s
 * @property UserListUniversity28[] $userListUniversity28s
 */
class MaketList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'maket_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['path'], 'required'],
            [['path'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserListUniversity1s()
    {
        return $this->hasMany(UserListUniversity1::className(), ['maket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserListUniversity28s()
    {
        return $this->hasMany(UserListUniversity28::className(), ['maket_id' => 'id']);
    }
}
