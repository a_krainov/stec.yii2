<?php

namespace app\models;

use Yii;
use app\models\Newserlistuniversity;
use yii\db\Schema;

/**
 * This is the model class for table "university_list".
 *
 * @property int $id
 * @property string $name
 * @property string $full_name
 *
 * @property UserListUniversity0[] $userListUniversity0s
 */
class UniversityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'university_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'full_name'], 'required'],
            [['name', 'full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'full_name' => 'Полное название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserListUniversity0s()
    {
        return $this->hasMany(UserListUniversity0::className(), ['university_id' => 'id']);
    }

    /**
     * При добавлении нового университета создаем отдельную таблицу
     */
    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        $flag = false;
        // Проверяем было ли сохренение изменением?
        foreach ($changedAttributes as $value)
        {
            if($value != ''){
                $flag = true;
            }
        }

        // если это новый элемент то создаем таблицу
        if(!$flag){
            if($this->id > 0){

                $arKeyTable = array(
                    'university_id' => 'university_list',
                    'department_id' => 'department_list',
                    'specialty_id' => 'specialty_list',
                    'maket_id' => 'maket_list',
                );

                $newTable = 'user_list_university_'.$this->id;

                Yii::$app->db->createCommand()->createTable('{{%'.$newTable.'}}', [
                   'id' => 'pk',
                   'date_add' => Schema::TYPE_TIMESTAMP.' NOT NULL',
                   'date_update' => Schema::TYPE_TIMESTAMP,
                   'surname' => Schema::TYPE_STRING.' NOT NULL',
                   'name' => Schema::TYPE_STRING.' NOT NULL',
                   'middle_name' => Schema::TYPE_STRING.' NOT NULL',
                   'university_id' => Schema::TYPE_BIGINT.' NOT NULL DEFAULT "0"', // Внешний ключ
                   'department_id' => Schema::TYPE_BIGINT.' NOT NULL DEFAULT "0"', // Внешний ключ
                   'specialty_id' => Schema::TYPE_BIGINT.' NOT NULL DEFAULT "0"', // Внешний ключ
                   'course' => Schema::TYPE_BIGINT.' NOT NULL DEFAULT "0"',
                   'validity' => Schema::TYPE_BIGINT.' NOT NULL DEFAULT "0"',
                   'date_birth' => Schema::TYPE_DATE.' NOT NULL',
                   'phone' => Schema::TYPE_STRING.' NOT NULL',
                   'form_training' => Schema::TYPE_STRING.' NOT NULL',
                   'print' => Schema::TYPE_SMALLINT.' NOT NULL DEFAULT "0"',
                   'card_id' => Schema::TYPE_BIGINT.' NOT NULL UNIQUE',
                   'comment' => Schema::TYPE_TEXT,
                   'foto' => Schema::TYPE_STRING.' NOT NULL',
                   'maket_id' => Schema::TYPE_BIGINT, // Внешний ключ
               ])->execute();


                // Составной индекс university_id
                foreach ($arKeyTable as $index => $value ){
                    if($index == 'maket_id'){
                        $type = 'SET NULL';
                    } else {
                        $type = null;
                    }
                    Yii::$app->db->createCommand()->createIndex(
                        'idx-'.$newTable.'-'.$index,
                        $newTable,
                        $index
                    )->execute();
                    Yii::$app->db->createCommand()->addForeignKey(
                        'idx-'.$newTable.'-'.$index,
                        $newTable,
                        $index,
                        $value,
                        'id',
                        $type
                    )->execute();
                }

            }
        }

    }

}
