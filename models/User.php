<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property int $status
 * @property int $role
 * @property int $university_id
 */
class User extends ActiveRecord implements IdentityInterface
{
    const ROLE_SUPERADMIN = 10;
    const ROLE_ADMIN = 20;
    const ROLE_USER = 30;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'login', 'email', 'password_hash'], 'required'],
            [['status', 'role', 'university_id'], 'integer'],
            [['name', 'login', 'email', 'password_hash'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['login'], 'unique'],
            [['email'], 'unique'],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniversityList::className(), 'targetAttribute' => ['university_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'login' => 'Логин',
            'email' => 'Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Пароль',
            'status' => 'Статус',
            'statusText' => 'Статус',
            'role' => 'Роль',
            'roleText' => 'Роль',
            'university_id' => 'Университет',
            'universityName' => 'Университет',
        ];
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
//        return static::findOne(['access_token' => $token]);
    }

    public static function findBylogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {


            $oldPass = $this->getOldAttribute('password_hash');
            $NewPass = $this->getAttribute('password_hash');

            if($NewPass != $oldPass){
                if ($insert) {
                    if ($this->password_hash != '') {
                        $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                        $this->auth_key = Yii::$app->security->generateRandomString();
                    }
                }

                if (!$insert && $this->password_hash) {
                    $this->password_hash = Yii::$app->security->generatePasswordHash($this->password_hash);
                    $this->auth_key = Yii::$app->security->generateRandomString();
                }
            }


            return true;
        }

        return false;
    }

    public function getUniversity()
    {
        return $this->hasOne(UniversityList::className(), ['id' => 'university_id']);
    }

    public function getRoleText()
    {
        if ($this->role == self::ROLE_ADMIN) {
            return 'Администратор';
        } elseif($this->role == self::ROLE_SUPERADMIN){
            return 'СуперАдминистратор';
        } else return 'Пользователь';
    }
    public function getStatusText()
    {
        if ($this->status == 1) {
            return 'Активен';
        } else return 'Неактивен';
    }

    /* Геттер для названия универа */
    public function getUniversityName() {
        return $this->university->name;
    }
}
