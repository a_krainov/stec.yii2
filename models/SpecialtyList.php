<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specialty_list".
 *
 * @property int $id
 * @property string $name
 */
class SpecialtyList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'specialty_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['university_id'], 'integer'],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniversityList::className(), 'targetAttribute' => ['university_id' => 'id']],
            [['department_id'], 'integer'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentList::className(), 'targetAttribute' => ['department_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'university_id' => 'Университет',
            'department_id' => 'Факультет',
            'universityName' => 'Университет',
            'departmentName' => 'Факультет',
        ];
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->university_id  == '') {
                $this->university_id = Yii::$app->user->identity->university_id;
            }


            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getUniversity()
    {
        return $this->hasOne(UniversityList::className(), ['id' => 'university_id']);
    }
    /* Геттер для названия универа */
    public function getUniversityName() {
        return $this->university->name;
    }
    /* Геттер для названия универа */
    public function getDepartmentName() {
        return $this->department->name_ru;
    }
    public function getDepartment()
    {
        return $this->hasOne(DepartmentList::className(), ['id' => 'department_id']);
    }


}
