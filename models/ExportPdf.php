<?php

namespace app\models;

use Yii;
use kartik\mpdf\Pdf;
use yii\helpers\FileHelper;

/**
 * Выгрузка в ПДФ
 */
class ExportPdf
{


    public static function export($data)
    {
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => 'Pdf::MODE_UTF8',
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $data,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => 'css/styles-print.css',
            //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            //'cssInline' => 'body {font-size:10px} .more {page-break-after: always;}',
            // set mPDF properties on the fly
            'options' => ['title' => 'maket-card'],
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Пример карты'],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public static function save($data, $model)
    {

        $path = 'uploads/maket/';
        $fullpath = 'uploads/maket/' . $model['university_id'] . '/';

        $filename = Yii::getAlias('@webroot/uploads/maket/').$model['university_id'].'/user_'.$model['id'].'.pdf';

        $file = $fullpath.'user_'.$model['id'].'.pdf';

        if (!is_dir($path)) {
            mkdir($path, 0755);
        }
        if (!is_dir($fullpath)) {
            mkdir($fullpath, 0755);
        }

        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => 'Pdf::MODE_UTF8',
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_FILE,
            // your html content input
            'content' => $data,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => 'css/styles-print.css',
            //'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            //'cssInline' => 'body {font-size:10px} .more {page-break-after: always;}',
            // set mPDF properties on the fly
            'options' => ['title' => 'maket-card'],
            'filename' => $filename,
            // call mPDF methods on the fly
            'methods' => [
                //'SetHeader'=>['Пример карты'],
                //'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        $pdf->render();

        // return the pdf output as per the destination setting
        return $file;
    }


}