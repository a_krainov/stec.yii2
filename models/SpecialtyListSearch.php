<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SpecialtyList;

/**
 * SpecialtyListSearch represents the model behind the search form of `app\models\SpecialtyList`.
 */
class SpecialtyListSearch extends SpecialtyList
{
    /* вычисляемый атрибут */
    public $universityName;
    public $departmentName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
            [['universityName'], 'safe'],
            [['departmentName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = SpecialtyList::find()->joinWith('university_name');
        $query = SpecialtyList::find();
        $idVus = Yii::$app->user->identity->university_id;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'universityName' => [
                    'asc' => ['university_list.name' => SORT_ASC],
                    'desc' => ['university_list.name' => SORT_DESC],
                    'label' => 'Вуз'
                ],
                'departmentName' => [
                    'asc' => ['department_list.name_ru' => SORT_ASC],
                    'desc' => ['department_list.name_ru' => SORT_DESC],
                    'label' => 'Факультет'
                ],
                'name',
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Страны
             * для работы сортировки.
             */
            $query->joinWith(['department']);

            //если юсер не суперадмин, то только свои ВУЗ
            $query->joinWith(['university' => function ($q) {
                    $q->where('university_list.id = "' . Yii::$app->user->identity->university_id . '"');
            }]);

            return $dataProvider;
        }

        /*$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/

        // grid filtering conditions
        $query->andFilterWhere([
            'specialty_list.id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'specialty_list.name', $this->name]);

        // Фильтр по Вузу

        $query->joinWith(['department' => function ($q) {
            $q->where('department_list.name_ru LIKE "%' . $this->departmentName . '%"');
        }]);


        $query->joinWith(['university' => function ($q) {
            $q->where('university_list.name LIKE "%' . $this->universityName . '%"');
            $q->andWhere('university_list.id = "' . Yii::$app->user->identity->university_id . '"');
        }]);


        return $dataProvider;
    }

}
