<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "user_list_university_0".
 *
 * @property int $id
 * @property string $date_add
 * @property string $date_update
 * @property string $surname
 * @property string $name
 * @property string $middle_name
 * @property int $university_id
 * @property int $department_id
 * @property int $specialty_id
 * @property int $course
 * @property int $validity
 * @property string $date_birth
 * @property string $phone
 * @property string $form_training
 * @property int $print
 * @property int $card_id
 * @property string $comment
 * @property string $foto
 * @property int $maket_id
 *
 * @property DepartmentList $department
 * @property UniversityList $university
 */
class Students extends \yii\db\ActiveRecord
{

    public static function getFormTraining()
    {
        return [
            '0' => 'бюджет',
            '1' => 'платная',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        $session = Yii::$app->session;
        // открываем сессию
        $session->open();

        if (Yii::$app->user->isGuest) {
            $session = Yii::$app->session;
            // открываем сессию
            $session->open();

            if (isset($session['university_id'])) {
                return 'user_list_university_'.$session['university_id'];
            }
        } else {
/*            if (Yii::$app->user->identity->role == User::ROLE_SUPERADMIN) {
                return 'user_list_university_1';
            } else if (Yii::$app->user->identity->role == User::ROLE_ADMIN) {*/
                return 'user_list_university_' . Yii::$app->user->identity->university_id;
            /*}*/
        }


    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname', 'name', 'middle_name', 'date_birth', 'phone', 'form_training', 'course', 'validity', 'department_id', 'specialty_id',], 'required'],
            [['date_add', 'date_update', 'date_birth'], 'safe'],
            [['university_id', 'department_id', 'specialty_id', 'course', 'print', 'card_id', 'maket_id'], 'integer'],
            [['validity'], 'integer', 'max' => 5],
            //[['comment', 'foto'], 'string'],
            [['comment'], 'string'],
            [['surname', 'name', 'middle_name', 'phone', 'form_training'], 'string', 'max' => 255],
            [['card_id'], 'unique'],
            //[['foto'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
            [['department_id'], 'exist', 'skipOnError' => true, 'targetClass' => DepartmentList::className(), 'targetAttribute' => ['department_id' => 'id']],
            [['university_id'], 'exist', 'skipOnError' => true, 'targetClass' => UniversityList::className(), 'targetAttribute' => ['university_id' => 'id']],
            [['specialty_id'], 'exist', 'skipOnError' => true, 'targetClass' => SpecialtyList::className(), 'targetAttribute' => ['specialty_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_add' => 'Создан',
            'date_update' => 'Изменён',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'middle_name' => 'Отчество',
            'university_id' => 'ВУЗ',
            'department_id' => 'Факультет',
            'specialty_id' => 'Специальность',
            'universityName' => 'ВУЗ',
            'departmentName' => 'Факультет',
            'specialtyName' => 'Специальность',
            'course' => 'Курс',
            'validity' => 'Период действия карты',
            'date_birth' => 'Дата рождения',
            'phone' => 'Телефон',
            'form_training' => 'Форма обучения',
            //'formtrainingName' => 'Форма обучения',
            'print' => 'Печать',
            'card_id' => 'Card ID',
            'foto' => 'Фото',
            'maket_id' => 'Maket ID',
            'comment' => 'Комментарий',
        ];
    }

    public function upload()
    {
        if (!is_dir('uploads/university/' . Yii::$app->user->identity->university_id . '/')) {
            mkdir('uploads/university/' . Yii::$app->user->identity->university_id . '/', 0755);
        }

        $path = 'uploads/university/' . Yii::$app->user->identity->university_id . '/' . self::transliteration($this->foto->baseName) . '_' . time() . '.' . $this->foto->extension;
        $this->foto->saveAs($path);
        $this->foto = $path;
        return true;

    }

    public static function transliteration($str, $flaglower = true)
    {
        // ГОСТ 7.79B
        $transliteration = array(
            'А' => 'A', 'а' => 'a',
            'Б' => 'B', 'б' => 'b',
            'В' => 'V', 'в' => 'v',
            'Г' => 'G', 'г' => 'g',
            'Д' => 'D', 'д' => 'd',
            'Е' => 'E', 'е' => 'e',
            'Ё' => 'Yo', 'ё' => 'yo',
            'Ж' => 'Zh', 'ж' => 'zh',
            'З' => 'Z', 'з' => 'z',
            'И' => 'I', 'и' => 'i',
            'Й' => 'J', 'й' => 'j',
            'К' => 'K', 'к' => 'k',
            'Л' => 'L', 'л' => 'l',
            'М' => 'M', 'м' => 'm',
            'Н' => "N", 'н' => 'n',
            'О' => 'O', 'о' => 'o',
            'П' => 'P', 'п' => 'p',
            'Р' => 'R', 'р' => 'r',
            'С' => 'S', 'с' => 's',
            'Т' => 'T', 'т' => 't',
            'У' => 'U', 'у' => 'u',
            'Ф' => 'F', 'ф' => 'f',
            'Х' => 'H', 'х' => 'h',
            'Ц' => 'Cz', 'ц' => 'cz',
            'Ч' => 'Ch', 'ч' => 'ch',
            'Ш' => 'Sh', 'ш' => 'sh',
            'Щ' => 'Shh', 'щ' => 'shh',
            'Ъ' => 'ʺ', 'ъ' => 'ʺ',
            'Ы' => 'Y`', 'ы' => 'y`',
            'Ь' => '', 'ь' => '',
            'Э' => 'E`', 'э' => 'e`',
            'Ю' => 'Yu', 'ю' => 'yu',
            'Я' => 'Ya', 'я' => 'ya',
            '№' => '#', 'Ӏ' => '‡',
            '’' => '`', 'ˮ' => '¨',
        );

        $str = strtr($str, $transliteration);

        if ($flaglower) {
            $str = mb_strtolower($str, 'UTF-8');
            $str = preg_replace('/[^0-9a-z\-]/', '', $str);
        } else {
            $str = preg_replace('/[^0-9a-zA-Z\-]/', '', $str);
        }

        $str = preg_replace('|([-]+)|s', '-', $str);
        $str = trim($str, '-');

        return $str;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($this->university_id == '') {
                $this->university_id = Yii::$app->user->identity->university_id;
            }
            if ($this->date_add == '') {
                $this->date_add = date('Y-m-d H:i:s');
                $this->date_update = date('Y-m-d H:i:s');
            }


            //Или изменения указываем дату
            if ($this->getOldAttribute('id') > 0) {
                $this->date_update = date('Y-m-d H:i:s');
            }

            // Меняем формат даты рождения
            $this->date_birth = Yii::$app->formatter->asDatetime($this->date_birth, "php:Y-m-d");

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->id > 0 && $this->card_id == 0) {
            // Код должен состоять из 13 цифр
            $firstValue = '5'; // 9 - система откуда создали карту
            $secondValue = str_pad(Yii::$app->user->identity->university_id, 4, "0", STR_PAD_LEFT); // 9999 - Код универа
            $threeValue = date('y'); // 99 - Год
            $fourValue = str_pad($this->id, 5, "0", STR_PAD_LEFT); //99 999 id студента

            $student = Students::findOne($this->id);
            $code = $firstValue . $secondValue . $threeValue . $fourValue;

            // Получаем код
            $newCode = new \app\controllers\CodeString();
            $barcode = $newCode->getInt($code,$newCode::TYPE_EAN_13);

            $student->card_id = $barcode;
            $student->save();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(DepartmentList::className(), ['id' => 'department_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUniversity()
    {
        return $this->hasOne(UniversityList::className(), ['id' => 'university_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialty()
    {
        return $this->hasOne(SpecialtyList::className(), ['id' => 'specialty_id']);
    }

    /* Геттер для названия */
    public function getUniversityName()
    {
        return $this->university->name;
    }

    public function getDepartmentName()
    {
        return $this->department->name_ru;
    }

    public function getSpecialtyName()
    {
        return $this->specialty->name;
    }

    public function getFormtrainingName()
    {
        $arData = self::getFormTraining();
        return $arData[$this->form_training];
    }

    public function getPrintStatus()
    {
        if ($this->print == 1) {
            return 'Да';
        } else return 'Нет';
    }

    public function getPrintStatusList()
    {
        return ['Нет', 'Да'];
    }
}
