<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Students;

/**
 * StudentsSearch represents the model behind the search form of `app\models\Students`.
 */
class StudentsSearch extends Students
{
    /* вычисляемый атрибут */
    public $universityName;
    public $departmentName;
    public $specialtyName;
    public $formtrainingName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'specialty_id', 'course', 'validity', 'print', 'card_id', 'maket_id'], 'integer'],
            [['date_add', 'date_update', 'surname', 'name', 'middle_name', 'date_birth', 'phone', 'form_training', 'comment', 'foto'], 'safe'],
            [['universityName'], 'safe'],
            [['departmentName'], 'safe'],
            [['specialtyName'], 'safe'],
            [['formtrainingName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Students::find();

        $idVus = Yii::$app->user->identity->university_id;
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'date_add',
                'date_update',
                'name',
                'surname',
                'middle_name',
                'surname',
                'course',
                'validity',
                'date_birth',
                'phone',
                'formtrainingName',
                'print',
                'card_id',
                'foto',
                'maket_id',
                'comment',
                'universityName' => [
                    'asc' => ['university_list.name' => SORT_ASC],
                    'desc' => ['university_list.name' => SORT_DESC],
                    'label' => 'Вуз'
                ],
                'departmentName' => [
                    'asc' => ['department_list.name_ru' => SORT_ASC],
                    'desc' => ['department_list.name_ru' => SORT_DESC],
                    'label' => 'Факультет'
                ],
                'specialtyName' => [
                    'asc' => ['specialty_list.name' => SORT_ASC],
                    'desc' => ['specialty_list.name' => SORT_DESC],
                    'label' => 'Специальность'
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            /**
             * Жадная загрузка данных модели Страны
             * для работы сортировки.
             */
            $query->joinWith(['department']);
            $query->joinWith(['specialty']);

            //если юсер не суперадмин, то только свои ВУЗ
            $query->joinWith(['university' => function ($q) {
                $q->where('university_list.id = "' . Yii::$app->user->identity->university_id . '"');
            }]);

            return $dataProvider;
        }
        /*$this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }*/

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            //'date_add' => $this->date_add,
            'date_update' => $this->date_update,
            //'university_id' => $this->university_id,
            'department_id' => $this->department_id,
            'specialty_id' => $this->specialty_id,
            'course' => $this->course,
            'validity' => $this->validity,
            'date_birth' => $this->date_birth,
            'print' => $this->print,
            'card_id' => $this->card_id,
            'maket_id' => $this->maket_id,
        ]);

        if($this->date_add != ''){
            $arrData = explode(' - ',$this->date_add);

            $dataTo = Yii::$app->formatter->asDate($arrData[0], "php:Y-m-d H:i:s");
            $dataFrom = Yii::$app->formatter->asDate($arrData[1], "php:Y-m-d H:i:s");

            $query->andFilterWhere(['>=', 'date_add', $dataTo])->andFilterWhere(['<=', 'date_add', $dataFrom]);
        }



        $query
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.surname', $this->surname])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.name', $this->name])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.middle_name', $this->middle_name])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.phone', $this->phone])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.form_training', $this->form_training])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.comment', $this->comment])
            ->andFilterWhere(['like', 'user_list_university_'.$idVus.'.foto', $this->foto]);


        // Фильтр по Вузу

        $query->joinWith(['department' => function ($q) {
            $q->where('department_list.name_ru LIKE "%' . $this->departmentName . '%"');
        }]);

        $query->joinWith(['specialty' => function ($q) {
           // $q->where('specialty_list.name LIKE "%' . $this->specialtyName . '%"');
            $q->andFilterWhere(['like', 'specialty_list.name', $this->specialtyName]);
        }]);

        $query->joinWith(['university' => function ($q) {
            //$q->where('university_list.name LIKE "%' . $this->universityName . '%"');
            //$q->andWhere('university_list.id = "' . Yii::$app->user->identity->university_id . '"');
            $q->andFilterWhere(['like', 'university_list.name', $this->universityName]);
            $q->andFilterWhere(['like', 'university_list.id', Yii::$app->user->identity->university_id]);
        }]);


        return $dataProvider;
    }
}
