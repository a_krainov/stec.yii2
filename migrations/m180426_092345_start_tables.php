<?php

use yii\db\Migration;

/**
 * Class m180426_092345_start_tables
 */
class m180426_092345_start_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_092345_start_tables cannot be reverted.\n";

        return false;
    }


    public function up()
    {
        // 1.   Пользователи (СуперАдмин, Админ, Специалист)
        $this->createTable('{{%user}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
            'login' => $this->string()->notNull()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'role' => $this->smallInteger()->notNull()->defaultValue(10),
            'university_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'settings' => $this->longtext(),
        ]);
        // 2.	Таблица «Студенты»
        $this->createTable('{{%user_list_university_1}}', [
            'id' => $this->bigPrimaryKey(),
            'date_add' => $this->timestamp()->notNull(),
            'date_update' => $this->timestamp(),
            'surname' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'middle_name' => $this->string()->notNull(),
            'university_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'department_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'specialty_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'course' => $this->bigInteger()->notNull()->defaultValue(0),
            'validity' => $this->bigInteger()->notNull()->defaultValue(0),
            'date_birth' => $this->date()->notNull(),
            'phone' => $this->string()->notNull(),
            'form_training' => $this->string()->notNull(),
            'print' => $this->smallInteger()->notNull()->defaultValue(0),
            'card_id' => $this->bigInteger()->notNull()->unique(),
            'comment' => $this->text(),
            'foto' => $this->string(255)->notNull(),
            'maket_id' => $this->bigInteger(), // Внешний ключ
        ]);
        // 3.	Таблица «Вуз»
        $this->createTable('{{%university_list}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
            'full_name' => $this->string()->notNull(),
        ]);

        // 4.	Таблица «Факультетов»
        $this->createTable('{{%department_list}}', [
            'id' => $this->bigPrimaryKey(),
            'name_ru' => $this->string()->notNull(),
            'name_en' => $this->string()->notNull(),
            'university_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
        ]);
        // 5.	Таблица «Специальностей»
        $this->createTable('{{%specialty_list}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull(),
            'university_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ
            'department_id' => $this->bigInteger()->notNull()->defaultValue(0), // Внешний ключ

        ]);
        // 6.	Таблица «готовых макетов»
        $this->createTable('{{%maket_list}}', [
            'id' => $this->bigPrimaryKey(),
            'path' => $this->string()->notNull(),
        ]);


    }

    public function down()
    {
        return false;
    }
}
