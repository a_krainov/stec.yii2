<?php

use yii\db\Migration;

/**
 * Class m180426_143537_start_tables
 */
class m180426_143537_start_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        // Состанвой индекс
        $this->createIndex(
            'idx-user_list_university_0-university_id',
            'user_list_university_0',
            'university_id'
        );
        // Состанвой индекс
        $this->createIndex(
            'idx-user_list_university_0-department_id',
            'user_list_university_0',
            'department_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'idx-user_list_university_0-university_id',
            'user_list_university_0',
            'university_id',
            'university_list',
            'id'
        );
        $this->addForeignKey(
            'idx-user_list_university_0-department_id',
            'user_list_university_0',
            'department_id',
            'department_list',
            'id'
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-user_list_university_0-specialty_id',
            'user_list_university_0',
            'specialty_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-user_list_university_0-specialty_id',
            'user_list_university_0',
            'specialty_id',
            'specialty_list',
            'id'
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-user_list_university_0-maket_id',
            'user_list_university_0',
            'maket_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-user_list_university_0-maket_id',
            'user_list_university_0',
            'maket_id',
            'maket_list',
            'id',
            'SET NULL'
        );


        // Состанвой индекс
        $this->createIndex(
            'idx-user-university_id',
            'user',
            'university_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-user-university_id',
            'user',
            'university_id',
            'university_list',
            'id'
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-department_list-university_id',
            'department_list',
            'university_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-department_list-university_id',
            'department_list',
            'university_id',
            'university_list',
            'id'
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-specialty_list-university_id',
            'specialty_list',
            'university_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-specialty_list-university_id',
            'specialty_list',
            'university_id',
            'university_list',
            'id'
        );

        // Состанвой индекс
        $this->createIndex(
            'idx-specialty_list-department_id',
            'specialty_list',
            'department_id'
        );

// add foreign key for table `user`
        $this->addForeignKey(
            'idx-specialty_list-department_id',
            'specialty_list',
            'department_id',
            'department_list',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180426_143537_start_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }
*/
    public function down()
    {
        echo "m180426_143537_start_tables cannot be reverted.\n";

        return false;
    }

}
