<?php

use yii\db\Migration;

/**
 * Class m180504_062958_start_tables
 */
class m180504_062958_start_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180504_062958_start_tables cannot be reverted.\n";

        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        // 1.   Параметры
        $this->createTable('{{%params}}', [
            'id' => $this->bigPrimaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'value' => $this->string(),
        ]);
    }

    public function down()
    {
        echo "m180504_062958_start_tables cannot be reverted.\n";

        return false;
    }

}
