<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UniversityList */

$this->title = 'Изменить : '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Списки университетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="university-list-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
