<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UniversityList */

$this->title = 'Создание университета';
$this->params['breadcrumbs'][] = ['label' => 'Списки университетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="university-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
