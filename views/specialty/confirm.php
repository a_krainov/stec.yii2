<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DepartmentList */

$this->title = 'Специальность добавлен';
?>
<div class="specialty-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

</div>
