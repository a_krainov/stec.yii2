<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpecialtyListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Списки Специальностей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialty-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="<?/*top-scroll-fix navbar-fixed-top*/?>">
        <?= Html::a('Создать специальность', ['create'], ['class' => 'btn btn-success']) ?>
        <?
        // Количество записей
        $values = Yii::$app->params['pagelist'];
        $current = $dataProvider->getPagination()->getPageSize();
        ?>
    <div class="dataTables_length pull-right" id="datatable_length"><label>Показывать по
            <select class="form-control" onchange="location = this.value;">
                <?php foreach ($values as $value): ?>
                    <option value="<?= Html::encode(Url::current(['per-page' => $value, 'page' => null])) ?>"
                            <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
                <?php endforeach; ?>
            </select> записи </label></div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'universityName',
            'departmentName',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
