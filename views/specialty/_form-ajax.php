<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UniversityList;
use app\models\DepartmentList;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialtyList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="specialty-list-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-specialty-add',
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?
    if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN){

        /* Получаем список Вузов*/
        $UniversityList = UniversityList::find()->asArray()->all();


        $arUniversitys = array();
        foreach($UniversityList as $val)
        {
            $arUniversitys[$val['id']] = $val['name'];
        }
        echo $form->field($model, 'university_id')->dropDownList($arUniversitys);
    }

    if (Yii::$app->user->identity->role == User::ROLE_SUPERADMIN) {
        $DepartementList = DepartmentList::find()->orderBy(['university_id' => SORT_ASC])->asArray()->all();

        $arDepartement = array();
        foreach ($DepartementList as $val) {
            $arDepartement[$val['id']] = $val['name_ru'].' ('. $arUniversitys[$val['university_id']] .')';
        }
    } else {
        $DepartementList = DepartmentList::find()->where(['university_id' => Yii::$app->user->identity->university_id])->asArray()->all();

        $arDepartement = array();
        foreach ($DepartementList as $val) {
            $arDepartement[$val['id']] = $val['name_ru'];
        }
    }

    echo $form->field($model, 'department_id')->dropDownList($arDepartement);

    ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('submit', 'form#form-specialty-add', function () {
            var form = $(this);
            form.find('input[type="submit"]').hide();
            // return false if form still have some validation errors
            if (form.find('.has-error').length)
            {
                form.find('input[type="submit"]').show();
                return false;
            }
            // submit form
            $.ajax({
                url    : form.attr('action'),
                type   : 'post',
                data   : form.serialize(),
                success: function (response)
                {
                    form.parents('.specialty-list-create').html(response);
                },
                error  : function ()
                {
                    console.log('internal server error');
                }
            });
            return false;
        });
    });
</script>