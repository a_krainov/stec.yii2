<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UniversityList;
use app\models\DepartmentList;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\SpecialtyList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="specialty-list-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?
    if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN){

        /* Получаем список Вузов*/
        $UniversityList = UniversityList::find()->asArray()->all();


        $arUniversitys = array();
        foreach($UniversityList as $val)
        {
            $arUniversitys[$val['id']] = $val['name'];
        }
        echo $form->field($model, 'university_id')->dropDownList($arUniversitys);
    }

    if (Yii::$app->user->identity->role == User::ROLE_SUPERADMIN) {
        $DepartementList = DepartmentList::find()->orderBy(['university_id' => SORT_ASC])->asArray()->all();

        $arDepartement = array();
        foreach ($DepartementList as $val) {
            $arDepartement[$val['id']] = $val['name_ru'].' ('. $arUniversitys[$val['university_id']] .')';
        }
    } else {
        $DepartementList = DepartmentList::find()->where(['university_id' => Yii::$app->user->identity->university_id])->asArray()->all();

        $arDepartement = array();
        foreach ($DepartementList as $val) {
            $arDepartement[$val['id']] = $val['name_ru'];
        }
    }

    echo $form->field($model, 'department_id')->dropDownList($arDepartement);

    ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
