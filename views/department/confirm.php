<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DepartmentList */

$this->title = 'Факультет добавлен';
?>
<div class="department-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

</div>
