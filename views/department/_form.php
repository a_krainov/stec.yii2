<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\UniversityList;


/* @var $this yii\web\View */
/* @var $model app\models\DepartmentList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-list-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?
    if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN){

        /* Получаем список Вузов*/
        $UniversityList = UniversityList::find()->all();

        $arUniversitys = array();
        foreach($UniversityList as $val)
        {
            $arUniversitys[$val->id] = $val->name;
        }
        echo $form->field($model, 'university_id')->dropDownList($arUniversitys);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
