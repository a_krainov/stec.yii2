<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\DepartmentList */

$this->title = 'Добавления факультета';
$this->params['breadcrumbs'][] = ['label' => 'Списки факультетов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin(['id' => 'my-pjax']);
?>
<div class="department-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form-ajax', [
        'model' => $model,
    ]) ?>

</div>
<?
Pjax::end();
?>
