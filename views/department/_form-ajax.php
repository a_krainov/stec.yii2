<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\UniversityList;

/* @var $this yii\web\View */
/* @var $model app\models\DepartmentList */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="department-list-form">

    <?php $form = ActiveForm::begin([
        'id' => 'form-department-add',
    ]); ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?
    if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN){

        /* Получаем список Вузов*/
        $UniversityList = UniversityList::find()->all();

        $arUniversitys = array();
        foreach($UniversityList as $val)
        {
            $arUniversitys[$val->id] = $val->name;
        }
        echo $form->field($model, 'university_id')->dropDownList($arUniversitys);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('submit', 'form#form-department-add', function () {
            var form = $(this);
            form.find('input[type="submit"]').hide();
            var name = $(this).find('#departmentlist-name_ru').val();
            // return false if form still have some validation errors
            if (form.find('.has-error').length)
            {
                form.find('input[type="submit"]').show();
                return false;
            }
            // submit form
            $.ajax({
                url    : form.attr('action'),
                type   : 'post',
                data   : form.serialize(),
                success: function (response)
                {
                    form.parents('.department-list-create').html(response);
                },
                error  : function ()
                {
                    console.log('internal server error');
                }
            });
            return false;
        });
    });
</script>