<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Восстановление пароля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php if (Yii::$app->session->hasFlash('forgotpasswordSubmitted')): ?>
        <div class="alert alert-success">
            <?
            $strSuccess = Yii::$app->params['settings']['successResetPassword'];
            $strSuccess = str_replace("#LOGIN#", $model->login, $strSuccess);
            ?>
           <?=$strSuccess?>
        </div>
    <?else:?>
        <p>Пожалуйста, укажите Ваш логин для восстановления пароля:</p>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>


        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                <?= Html::a('Вход', ['site/login'], ['class' => 'btn btn-link']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>
    <?endif;?>


</div>
