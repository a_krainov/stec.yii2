<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\db\Query;

$this->title = 'Стек ПОИСК';

$value = '';
$result = 0;

if (isset($searchModel['search'])) {
    if ($searchModel['search'] != '') {
        $value = $searchModel['search'];

        if (Yii::$app->user->isGuest) {
            /*$model = Yii::$app->db->createCommand('SELECT * FROM user_list_university_1 WHERE card_id=5001201800035')
                ->queryOne();*/

            $arUniversity = \app\models\UniversityList::find()->asArray()->all();


            foreach ($arUniversity as $University) {

                $rows = (new Query())->select(['*'])->from('user_list_university_'.$University['id'])->where(['=', 'card_id', $searchModel['search']]);
                $command = $rows->createCommand();
                $model = $command->queryOne();

                if (!empty($model) && count($model) > 0) {

                    $session = Yii::$app->session;
                    // открываем сессию
                    $session->open();
                    $session['university_id'] = $model['university_id'];

                    $model = \app\models\Students::find()->where(['=', 'card_id', $searchModel['search']])->one();
                    $result = count($model);
                    break;
                } else {
                    $result = 0;
                }
            }


        } else {
            $model = \app\models\Students::find()->where(['=', 'card_id', $searchModel['search']])->one();
            $result = count($model);
        }

    }
}

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>База студентов СТЕК!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-md-12">
                <?php $form = ActiveForm::begin([
                    'action' => ['index'],
                    'method' => 'post',
                    'options' => ['class' => 'example'],
                ]); ?>
                <input type="text" placeholder="Поиск.." name="search" value="<?= $value ?>" autofocus>
                <input type="hidden" name="form-main-page" value="1">
                <?= Html::submitButton('<span class="glyphicon glyphicon-search" aria-hidden="true"></span>', ['class' => 'btn btn-primary']) ?>

                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-6 student-info">

                <? if (isset($searchModel['search'])) { ?>
                    <h1>Результат поиска</h1>
                    <?
                    if ($result == 1) {
                        ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'surname',
                                'name',
                                'middle_name',
                                'universityName',
                                'departmentName',
                                'specialtyName',
                                'course',
                                [
                                    'attribute'=>'Фото',
                                    'value'=>$model->foto,
                                    'format' => ['image',],
                                ],
                            ],
                        ]) ?>

                    <? } else { ?>
                        <p class="error-summary">Не чего не найдено.</p>
                    <?
                    }
                }
                ?>
            </div>
        </div>
        <style>
            * {
                box-sizing: border-box;
            }

            .student-info {
                margin-top: 20px;
            }

            /* Style the search field */
            form.example input[type=text] {
                padding: 10px;
                font-size: 17px;
                border: 1px solid grey;
                float: left;
                width: 80%;
                background: #f1f1f1;
            }

            /* Style the submit button */
            form.example button {
                float: left;
                width: 20%;
                padding: 10px;
                background: #2196F3;
                color: white;
                font-size: 17px;
                border: 1px solid grey;
                border-left: none; /* Prevent double borders */
                cursor: pointer;
                border-top-left-radius: 0;
                border-bottom-left-radius: 0;
            }

            form.example button:hover {
                background: #0b7dda;
            }

            /* Clear floats */
            form.example::after {
                content: "";
                clear: both;
                display: table;
            }
        </style>
    </div>
</div>
