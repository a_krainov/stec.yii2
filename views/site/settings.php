<?php

/* @var $this yii\web\Settings */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\base\ErrorException;

Pjax::begin(['id' => 'my-pjax']);
?>
<?php $form = ActiveForm::begin(['id' => 'settingsform', 'action' => '', 'options' => ['data-pjax' => true]]); ?>
<?php //$form = ActiveForm::begin(['id' => 'settingsform']); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">?</span>
    </button>
    <h4 class="modal-title" id="myModalLabel">Настройки представления</h4>
</div>
<div class="modal-body">
    <? if (isset($result) && $result) { ?>
        <div class="alert alert-success" role="alert">
            Сохранены настройки
        </div>
    <? } ?>
    <div class="container" style="margin-top:20px;">
        <div class="row">
            <div class="col-xs-12">
                <div class="well" style="max-height: 600px;overflow: auto;">

                    <ul class="list-group checked-list-box table-sort-ul">
                        <? foreach ($UserModelSettings as $Settings) {
                            if(array_key_exists($Settings, $attributeLabels)){?>
                            <li class="list-group-item"><label class="custom-control custom-checkbox">
                                    <input type="checkbox" checked class="custom-control-input"
                                           name="settingsForm[]" value="<?= $Settings ?>">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description"><?= $attributeLabels[$Settings] ?></span>
                                </label>
                            </li>
                            <?
                            unset($attributeLabels[$Settings]);
                            }
                        } ?>
                        <? foreach ($attributeLabels as $index => $Labels) {
                            ?>
                            <li class="list-group-item"><label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="settingsForm[]"
                                           value="<?= $index ?>">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description"><?= $Labels ?></span>
                                </label>
                            </li>
                            <?
                        } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
</div>
<?php ActiveForm::end(); ?>
<?
Pjax::end();
?>
<?
$this->registerJs("
$('#settingsform .table-sort-ul').sortable();
");

?>

