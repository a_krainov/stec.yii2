<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div>
        <?= Html::a('Создать пользователя', ['create'], ['class' => 'btn btn-success']) ?>

        <?
        // Количество записей
        $values = Yii::$app->params['pagelist'];
        $current = $dataProvider->getPagination()->getPageSize();
        ?>
    <div class="dataTables_length pull-right" id="datatable_length"><label>Показывать по
            <select class="form-control" onchange="location = this.value;">
                <?php foreach ($values as $value): ?>
                    <option value="<?= Html::encode(Url::current(['per-page' => $value, 'page' => null])) ?>"
                            <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
                <?php endforeach; ?>
            </select> записи </label></div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'login',
            'email:email',
            'statusText',
            'roleText',
            'universityName',
            //'auth_key',
            //'password_hash',
            //'role',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
