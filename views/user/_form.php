<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

/* Получаем список Вузов*/
$UniversityList = \app\models\UniversityList::find()->all();

$arUniversitys = array();
foreach($UniversityList as $val)
{
    $arUniversitys[$val->id] = $val->name;
}
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?//= $form->field($model, 'password_hash')->textInput(['maxlength' => true]) ?>
    <div class="form-group field-user-password_hash required">
        <label class="control-label" for="user-password_hash">Пароль</label>
        <input type="text" id="user-password_hash" class="form-control" name="User[password_hash]" value="" maxlength="255" aria-required="true">

        <div class="help-block"></div>
    </div>
    <?//= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'role')->dropDownList([ User::ROLE_SUPERADMIN => 'СуперАдминистратор', User::ROLE_ADMIN => 'Администратор', User::ROLE_USER => 'Пользователь']) ?>
    <?= $form->field($model, 'university_id')->dropDownList($arUniversitys) ?>

    <?= $form->field($model, 'status')->checkbox(['checked ' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
