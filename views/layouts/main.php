<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\User;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    //задаём пунты меню в зависимости от роли пользователя
    $items = [];
    if (Yii::$app->user->isGuest) {
        $items[] = ['label' => 'Главная', 'url' => ['/']];
        $items[] = ['label' => 'Вход', 'url' => ['/site/login']];
        $items[] = ['label' => 'Контакты', 'url' => ['/site/contact']];
    } else {
        if (Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            $items[] = ['label' => 'Главная', 'url' => ['/']];
            $items[] =  ['label' => 'Студенты', 'url' => ['/students/index']];
            $items[] =  ['label' => 'Факультеты', 'url' => ['/department/index']];
            $items[] =  ['label' => 'Специальности', 'url' => ['/specialty/index']];
            $items[] =  '<li>'. Html::beginForm(['/site/logout'], 'post'). Html::submitButton('Выход (' . Yii::$app->user->identity->login . ')',['class' => 'btn btn-link logout']). Html::endForm(). '</li>';
        } elseif (Yii::$app->user->identity->role == User::ROLE_USER) {
            $items[] = ['label' => 'Главная', 'url' => ['/']];
            $items[] =  ['label' => 'Студенты', 'url' => ['/students/index']];
            $items[] =  '<li>'. Html::beginForm(['/site/logout'], 'post'). Html::submitButton('Выход (' . Yii::$app->user->identity->login . ')',['class' => 'btn btn-link logout']). Html::endForm(). '</li>';
        } elseif (Yii::$app->user->identity->role == User::ROLE_SUPERADMIN) {
            $items[] = ['label' => 'Главная', 'url' => ['/']];
            $items[] =  ['label' => 'Пользователи', 'url' => ['/user/index']];
            $items[] =  ['label' => 'Студенты', 'url' => ['/students/index']];
            $items[] =  ['label' => 'Вузы', 'url' => ['/university/index']];
            $items[] =  ['label' => 'Факультеты', 'url' => ['/department/index']];
            $items[] =  ['label' => 'Специальности', 'url' => ['/specialty/index']];
            $items[] =  ['label' => 'Настройки', 'url' => ['/params/index']];
            $items[] =  '<li>'. Html::beginForm(['/site/logout'], 'post'). Html::submitButton('Выход (' . Yii::$app->user->identity->login . ')',['class' => 'btn btn-link logout']). Html::endForm(). '</li>';
        }
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);

    /*echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Главная', 'url' => ['/site/index']],
                ['label' => 'Пользователи', 'url' => ['/user/index']],
                ['label' => 'Список Студентов', 'url' => ['/students/index']],
                ['label' => 'Список Вузов', 'url' => ['/university/index']],
                ['label' => 'Список Факультетов', 'url' => ['/department/index']],
                ['label' => 'Список Специальностей', 'url' => ['/specialty/index']],
                Yii::$app->user->isGuest ? (
                ['label' => 'Вход', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        'Выход (' . Yii::$app->user->identity->login . ')',
                        ['class' => 'btn btn-link logout']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ],
        ]);*/
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Профком ПетрГУ <?= date('Y') ?></p>

        <p class="pull-right"><?//= Yii::powered() ?></p>
    </div>
</footer>
<!-- Modal -->
<div class="modal fade" id="Settings" tabindex="-1" role="dialog" aria-labelledby="Settings">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<?
$this->registerJs("    
    $('.grid-view tbody tr').on('dblclick', function() {
        $(this).find('span.glyphicon-eye-open').click();
    });
")
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
