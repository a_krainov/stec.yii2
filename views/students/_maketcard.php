<?php

use yii\helpers\Html;
use \app\models\Students;
use \app\models\UniversityList;
use \app\models\DepartmentList;

/* @var $this yii\web\View */
/* @var $model app\models\Students */
/* @var $form yii\widgets\ActiveForm */
$arResult = array(
    'Students[university_id]' => 1,
    'Students[university_name]' => '',
    'Students[surname]' => '',
    'Students[name]' => '',
    'Students[middle_name]' => '',
    'Students[department_name_ru]' => '',
    'Students[department_name_en]' => '',
    'Students[validity]' => '1',
    'Students[date_birth]' => '',
    'Students[form_training]' => 0,
    'Students[foto]' => '/i/photo.jpg',
);


foreach ($model as $item) {
    if ($item['value'] != '') {
        $arResult[$item['name']] = $item['value'];
    }
}
// Получаем название Вуза
if (isset($arResult['Students[university_id]'])) {
    $Vus = UniversityList::find()->where(['id' => $arResult['Students[university_id]']])->asArray()->one();
    if ($Vus['name'] != '') {
        $arResult['Students[university_name]'] = $Vus['name'];
    } elseif ($Vus['full_name']) {
        $arResult['Students[university_name]'] = $Vus['full_name'];
    }
}

if (isset($arResult['Students[department_id]'])) {
    $dem = DepartmentList::find()->where(['id' => $arResult['Students[department_id]']])->asArray()->one();

    if ($dem['name_ru'] != '') {
        $arResult['Students[department_name_ru]'] = $dem['name_ru'];
    }
    if ($dem['name_en']) {
        $arResult['Students[department_name_en]'] = $dem['name_en'];
    }
}
if(isset($arResult['Students[validity]'])){
    if($arResult['Students[validity]'] > 5){
        $arResult['Students[validity]'] = 5;
    }
}
?>
<div class="card">
    <div class="card__top">
        <div class="card__photo">
            <img src="/i/photo.jpg" alt="">
        </div>
        <div class="card__info">
            <div class="card__info-top">
                <div class="card__stek-logo">
                    <img src="/i/stek-logo-color.svg" alt="">
                </div>
                <div class="card__tu-logo">
                    <img src="/i/tu-logo.svg" alt="">
                </div>
                <div class="card__barcode">
                    <img src="/i/barcode.png" alt="">
                </div>
            </div>
            <div class="card__info-header">
                <strong>Студенческая электронная карта</strong><br>
                Student identity card | <a href="http://stec.me">http://stec.me</a>
            </div>
            <div class="card__name-ru">
                <? if ($arResult['Students[form_training]'] == 0){?>
                <?= $arResult['Students[surname]'] ?><br>
                <?= $arResult['Students[name]'] ?> <?= $arResult['Students[middle_name]'] ?>
                <? } else {?>
                <b><?= $arResult['Students[surname]'] ?></b><br>
                <b><?= $arResult['Students[name]'] ?></b> <b><?= $arResult['Students[middle_name]'] ?></b>
                <?}?>
            </div>
            <div class="card__name-en">
                <?= Students::transliteration($arResult['Students[surname]'], false) ?> <?= Students::transliteration($arResult['Students[name]'], false) ?>
            </div>
            <div class="card__props">
                Дата рождения: <span><?= $arResult['Students[date_birth]'] ?></span>
            </div>
            <div class="card__props">
                Факультет: <span><?= $arResult['Students[department_name_ru]'] ?></span>
            </div>
            <div class="card__props">
                Department: <span><?= $arResult['Students[department_name_en]'] ?></span>
            </div>
        </div>
    </div>
    <div class="card__bottom">
        <span class="card__flags">
            <div class="card__flag card__flag_ru">
                <img src="/i/flag-ru.png" alt="">
            </div>
            <div class="card__flag card__flag_eu">
                <img src="/i/flag-eu.png" alt="">
            </div>
            <div class="card__flags-note">valid in EU</div>
        </span>
        <span class="card__valid">
            <span>действительна до 31 октября</span>
        </span>
        <span class="card__years">
            <? for ($colomn = 0; $colomn < $arResult['Students[validity]']; $colomn++) { ?>
                <div class="card__year">
                    <span><?= date('Y', strtotime('+' . $colomn . ' year')) ?></span>
                    <img src="/i/stek-logo-bw.svg" alt="">
                </div>
            <? } ?>
        </span>
    </div>
    <div class="card__univercity-name">
        <svg id="svgRoot" class="content" preserveAspectRatio="xMidYMid meet" viewBox="0 0 50 20">
            <text id="text" x="0" y="0" font-size="14" dy="0" fill="#fff" font-family="MyriadPro, sans-serif">
                <tspan x="0" y="14"><?= $arResult['Students[university_name]'] ?></tspan>
            </text>
        </svg>
    </div>
</div>