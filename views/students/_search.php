<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use app\models\UniversityList;
use app\models\DepartmentList;

/* @var $this yii\web\View */
/* @var $model app\models\StudentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-md-4">
        <div class="user-list-search">

            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'post',
            ]); ?>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3">
                        <label class="control-label">Сменить ВУЗ</label>
                    </div>
                    <div class="col-md-6 no-padding">
                        <?
                        // The controller action that will render the list
                        $urluniversity = \yii\helpers\Url::to(['university-ajax']);
                        // Get the initial universityList description
                        $universityName = UniversityList::findOne(Yii::$app->user->identity->university_id)->name;

                        echo $form->field($model, 'university_id')->widget(Select2::classname(), [
                            'initValueText' => $universityName, // set the initial display text
                            //'options' => ['placeholder' => 'Выбирите унивирситет ...'],
                            'pluginOptions' => [
                                'initialize' => true,
                                'allowClear' => false,
                                'minimumInputLength' => 0,
                                'language' => 'ru',
                                'ajax' => [
                                    'url' => $urluniversity,
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(university) { return university.text; }'),
                                'templateSelection' => new JsExpression('function (university) { return university.text; }'),
                            ],
                        ])->label(false);
                        ?>
                    </div>
                    <div class="col-md-3">
                            <input type="hidden" name="change_university_id" value="Y">
                            <?= Html::submitButton('Изменить', ['class' => 'btn btn-primary']) ?>
                            <? //= Html::resetButton('Вернуть по умолчанию', ['class' => 'btn btn-default']) ?>
                    </div>
                </div>
            </div>





            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
