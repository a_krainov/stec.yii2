<?php

use yii\helpers\Html;
use \app\models\Students;
use \app\models\UniversityList;
use \app\models\DepartmentList;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

/* @var $this yii\web\View */
/* @var $model app\models\Students */
/* @var $form yii\widgets\ActiveForm */
$arResult = array(
    'Students[university_id]' => 1,
    'Students[university_name]' => '',
    'Students[surname]' => '',
    'Students[name]' => '',
    'Students[middle_name]' => '',
    'Students[department_name_ru]' => '',
    'Students[department_name_en]' => '',
    'Students[validity]' => '1',
    'Students[date_birth]' => '',
    'Students[form_training]' => 0,
    'Students[foto]' => '/i/photo.jpg',
);

if(isset($model)){
    foreach ($model as $index => $item) {
        if ($item != '') {
            $arResult['Students['.$index.']'] = $item;
        }
    }
}

// Получаем название Вуза
if (isset($arResult['Students[university_id]'])) {
    $Vus = UniversityList::find()->where(['id' => $arResult['Students[university_id]']])->asArray()->one();
    if ($Vus['name'] != '') {
        $arResult['Students[university_name]'] = $Vus['name'];
    } elseif ($Vus['full_name']) {
        $arResult['Students[university_name]'] = $Vus['full_name'];
    }
}

if (isset($arResult['Students[department_id]'])) {
    $dem = DepartmentList::find()->where(['id' => $arResult['Students[department_id]']])->asArray()->one();

    if ($dem['name_ru'] != '') {
        $arResult['Students[department_name_ru]'] = $dem['name_ru'];
    }
    if ($dem['name_en']) {
        $arResult['Students[department_name_en]'] = $dem['name_en'];
    }
}
if(isset($arResult['Students[validity]'])){
    if($arResult['Students[validity]'] > 5){
        $arResult['Students[validity]'] = 5;
    }
}
$thumbnail = Yii::getAlias('@webroot/test_thumb.jpg');

Image::getImagine()->open(Yii::getAlias('@webroot/').$arResult['Students[foto]'])->thumbnail(new Box(200, 124))->save( $thumbnail, ['quality' => 100]);

Image::thumbnail('@webroot/'.$arResult['Students[foto]'], 100, 124)
    ->save($thumbnail, ['quality' => 100]);

//<link href="/css/styles-print.css" rel="stylesheet">
/*
 <html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" href="https://stec.pixelproject.ru/css/styles-print.css" rel="stylesheet" />
</head>
<body>
 */
?>
<link href="/css/styles-print.css" rel="stylesheet">
<?/*<div class="card">
    <div class="card__top">
        <div class="card__photo">
            <img src="/<?= $thumbnail //$arResult['Students[foto]'] ?>" alt="">
        </div>
        <div class="card__info">
            <div class="card__info-top">
                <div class="card__stek-logo">
                    <img src="/i/stek-logo-color.png" alt="">
                </div>
                <div class="card__tu-logo">
                    <img src="/i/tu-logo.png" alt="">
                </div>
                <div class="card__barcode">
                    <?
                    $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                    echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($arResult['Students[card_id]'], $generator::TYPE_EAN_13, '1', '50')) . '">';
                    ?>
                </div>
            </div>
            <div class="card__info-header">
                <strong>Студенческая электронная карта</strong><br>
                Student identity card | <a href="http://stec.me">http://stec.me</a>
            </div>
            <div class="card__name-ru">
                <? if ($arResult['Students[form_training]'] == 0){?>
                    <?= $arResult['Students[surname]'] ?><br>
                    <?= $arResult['Students[name]'] ?> <?= $arResult['Students[middle_name]'] ?>
                <? } else {?>
                    <b><?= $arResult['Students[surname]'] ?></b><br>
                    <b><?= $arResult['Students[name]'] ?></b> <b><?= $arResult['Students[middle_name]'] ?></b>
                <?}?>
            </div>
            <div class="card__name-en">
                <?= Students::transliteration($arResult['Students[surname]'], false) ?> <?= Students::transliteration($arResult['Students[name]'], false) ?>
            </div>
            <div class="card__props">
                Дата рождения: <span><?= $arResult['Students[date_birth]'] ?></span>
            </div>
            <div class="card__props">
                Факультет: <span><?= $arResult['Students[department_name_ru]'] ?></span>
            </div>
            <div class="card__props">
                Department: <span><?= $arResult['Students[department_name_en]'] ?></span>
            </div>
        </div>
    </div>
     <div class="card__bottom">
        <div class="card__flags">
            <div class="card__flag card__flag_ru"></div>
            <div class="card__flag card__flag_eu"></div>
            <div class="card__flags-note">valid in EU</div>
        </div>

        <div class="card__years">
            <? for ($colomn = 0; $colomn < $arResult['Students[validity]']; $colomn++) { ?>
                <div class="card__year">
                    <span><?= date('Y', strtotime('+' . $colomn . ' year')) ?></span>
                    <img src="/i/stek-logo-bw.png" alt="">
                </div>
            <? } ?>
        </div>
    </div>
</div>
<div class="card__valid">
    <span>действительна до 31 октября</span>
</div>
<div class="card__univercity-name" >
    <svg id="svgRoot" class="content" preserveAspectRatio="xMidYMid meet" viewBox="0 0 50 20">
        <text id="text" x="0" y="0" font-size="14" dy="0">
            <tspan x="0" y="14"><?= $arResult['Students[university_name]'] ?></tspan>
        </text>
    </svg>
</div>
*/?>
<table class="card">
    <tr>
        <td rowspan="2" colspan="2" class="card__photo-td">
            <img src="/<?= $thumbnail //$arResult['Students[foto]'] ?>" alt="" class="card__photo">
        </td>
        <td class="card__stek-logo-td">
            <img src="i/stek-logo-color.png" alt="" class="card__stek-logo">
        </td>
        <td class="card__tu-logo-td">
            <img src="i/tu-logo.png" alt="" class="card__tu-logo">
        </td>
        <td class="card__barcode-td">
            <barcode code="<?=$arResult['Students[card_id]']?>" size="0.5" height="0.6" class="barcode card__barcode" />
            <?/*
            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
            echo '<img class="card__barcode" src="data:image/png;base64,' . base64_encode($generator->getBarcode($arResult['Students[card_id]'], $generator::TYPE_EAN_13, '1', '50')) . '">';
            */?>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="card__info-td">
            <div class="card__info-header">
                <strong>Студенческая электронная карта</strong><br>
                Student identity card | <a href="http://stec.me" class="card__info-header-link">http://stec.me</a>
            </div>
            <div class="card__name-ru">
                <? if ($arResult['Students[form_training]'] == 0){?>
                    <?= $arResult['Students[surname]'] ?><br>
                    <?= $arResult['Students[name]'] ?> <?= $arResult['Students[middle_name]'] ?>
                <? } else {?>
                    <b><?= $arResult['Students[surname]'] ?></b><br>
                    <b><?= $arResult['Students[name]'] ?></b> <b><?= $arResult['Students[middle_name]'] ?></b>
                <?}?>
            </div>
            <div class="card__name-en">
                <?= Students::transliteration($arResult['Students[surname]'], false) ?> <?= Students::transliteration($arResult['Students[name]'], false) ?>
            </div>
            <div class="card__props">
                Дата рождения: <span><?= $arResult['Students[date_birth]'] ?></span>
            </div>
            <div class="card__props">
                Факультет: <span><?= $arResult['Students[department_name_ru]'] ?></span>
            </div>
            <div class="card__props">
                Department: <span><?= $arResult['Students[department_name_en]'] ?></span>
            </div>
            <div class="card__univercity-name">
                <svg class="card__univercity-name-svg content" id="svgRoot" preserveAspectRatio="xMidYMid meet" viewBox="0 0 50 20">
                    <text id="text" x="0" y="0" font-size="14" dy="0" fill="#fff">
                        <tspan x="0" y="14"><? = $arResult['Students[university_name]'] ?></tspan>
                    </text>
                </svg>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="5">
            <table class="card__bottom">
                <tr>
                    <td class="card__flags-td">
                        <img src="i/flag-ru.png" alt="" class="card__flag">
                        <img src="i/flag-eu.png" alt="" class="card__flag">
                        <div class="card__flags-note">valid in EU</div>
                    </td>
                    <td class="card__valid-td">
                        <div class="card__valid-div">
                            действительна
                            до 31 октября
                        </div>
                    </td>
                    <td class="card__valid-td">
                    </td>
                    <td class="card__years-td">
                        <table class="card-years-table">
                            <tr>
                            <? for ($colomn = 0; $colomn < $arResult['Students[validity]']; $colomn++) { ?>
                                    <td class="card__year-text-td">
                                        <?= date('Y', strtotime('+' . $colomn . ' year')) ?>
                                    </td>
                            <? } ?>
                            </tr>
                            <tr>
                                <? for ($colomn = 0; $colomn < $arResult['Students[validity]']; $colomn++) { ?>
                                    <td class="card__year-img-td">
                                        <img src="i/stek-logo-bw.png" alt="" class="card__year-img">
                                    </td>
                                <? } ?>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?
/*<barcode code="<?=$arResult['Students[card_id]']?>" size="0.5" class="barcode" />*/
?>





