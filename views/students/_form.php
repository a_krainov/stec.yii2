<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\jui\DatePicker;
use app\models\User;
use app\models\UniversityList;
use app\models\DepartmentList;
use app\models\SpecialtyList;
use app\models\Students;
use kartik\select2\Select2;
use yii\web\JsExpression;
use kartik\date\DatePicker;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\Students */
/* @var $form yii\widgets\ActiveForm */

// Типы обучений
$typesFormTraining = Students::getFormTraining();
?>
<script>

</script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="user-list-form">

                <?php $form = ActiveForm::begin(['id' => 'form-students-add', 'options' => ['enctype' => 'multipart/form-data']]); ?>


                <?php if (!$model->isNewRecord): ?>
                    <? echo $form->field($model, 'university_id')->hiddenInput()->label(false); ?>
                    <?= $form->field($model, 'date_add')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>
                    <?php if ($model->date_update != ''): ?>
                        <?= $form->field($model, 'date_update')->textInput(['value' => date('Y-m-d H:i:s'), 'readonly' => true]) ?>
                    <? endif; ?>
                    <?= $form->field($model, 'card_id')->textInput(['readonly' => true]) ?>
                <? else: ?>
                    <input type="hidden" id="students-university_id" class="form-control" name="Students[university_id]"
                           value="<?= Yii::$app->user->identity->university_id ?>">
                <? endif; ?>

                <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

                <?
                /*    if(Yii::$app->user->identity->role == User::ROLE_SUPERADMIN){

                        // Получаем список Вузов
                        $UniversityList = UniversityList::find()->all();

                        $arUniversitys = array();
                        foreach($UniversityList as $val)
                        {
                            $arUniversitys[$val->id] = $val->name;
                        }
                        echo $form->field($model, 'university_id')->dropDownList($arUniversitys);
                    }*/
                ?>

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 no-padding">
                            <?
                            // The controller action that will render the list
                            $urldepartment = \yii\helpers\Url::to(['department-ajax']);
                            // Get the initial DepartmentList description
                            $departmentName = empty($model->department_id) ? '' : DepartmentList::findOne($model->department_id)->name_ru;

                            echo $form->field($model, 'department_id')->widget(Select2::classname(), [
                                'initValueText' => $departmentName, // set the initial display text
                                'options' => ['placeholder' => 'Выбирите факультет ...'],
                                'pluginOptions' => [
                                    'initialize' => true,
                                    'allowClear' => true,
                                    'minimumInputLength' => 0,
                                    'language' => 'ru',
                                    'ajax' => [
                                        'url' => $urldepartment,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(department) { return department.text; }'),
                                    'templateSelection' => new JsExpression('function (department) { return department.text; }'),
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Добавить</label>
                            <a id="modal-btn" class="btn btn-info modal-add" data-href="/department/add-ajax"
                               data-target="#modal-department">+</a>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-10 no-padding">
                            <?
                            // The controller action that will render the list
                            $urlspecialty = \yii\helpers\Url::to(['specialty-ajax']);

                            // Get the initial DepartmentList description
                            $specialtyName = empty($model->specialty_id) ? '' : SpecialtyList::findOne($model->specialty_id)->name;
                            echo $form->field($model, 'specialty_id')->widget(Select2::classname(), [
                                'initValueText' => $specialtyName, // set the initial display text
                                'options' => ['placeholder' => 'Выбирите специальность...'],
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 0,
                                    'language' => 'ru',
                                    'ajax' => [
                                        'url' => $urlspecialty,
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term, department:$("#students-department_id").val()}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(specialty) { return specialty.text; }'),
                                    'templateSelection' => new JsExpression('function (specialty) { return specialty.text; }'),
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-2">
                            <label class="control-label">Добавить</label>
                            <a id="modal-btn" class="btn btn-info modal-add" data-href="/specialty/add-ajax"
                               data-target="#modal-specialty">+</a>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'course')->textInput() ?>

                <?= $form->field($model, 'validity')->textInput(['type' => 'number', 'max' => 5]) ?>

                <?
                /*                    <div class="form-group field-students-date_birth required">
                                    <label class="control-label" for="students-date_birth">Дата рождения</label>
                    echo DatePicker::widget([
                                        'name' => 'Students[date_birth]',
                                        'value' => $model->date_birth,
                                        'language' => 'ru',
                                        'dateFormat' => 'dd-MM-yyyy',
                                        'options' => ['placeholder' => 'Выберите дату ...', 'class' => 'form-control'],
                                    ]);*/

                echo $form->field($model, 'date_birth')->widget(DatePicker::classname(), [
                    //'name' => 'Students[date_birth]',
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'value' => $model->date_birth,
                    'options' => ['placeholder' => 'Выберите дату ...', 'class' => 'form-control'],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ],
                ]);

                /* echo DatePicker::widget([
                     'name' => 'Students[date_birth]',
                     'type' => DatePicker::TYPE_COMPONENT_APPEND,
                     'value' => $model->date_birth,
                     'options' => ['placeholder' => 'Выберите дату ...', 'class' => 'form-control'],
                     'pluginOptions' => [
                         'autoclose'=>true,
                         'format' => 'dd.mm.yyyy'
                     ],
                 ]);
                 <div class="help-block"></div>
             </div>
*/

                ?>
                <? //= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
                    'mask' => '+7 (999) 999 99 99',
                ])->textInput(['placeholder' => $model->getAttributeLabel('phone')]); ?>

                <?= $form->field($model, 'form_training')->radioList($typesFormTraining) ?>


                <? //= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'foto')->fileInput() ?>
                <div class="form-group">
                    <button type="button" class="btn btn-default foto-camera">
                        <span class="glyphicon glyphicon-camera"></span> Фото с камеры
                    </button>
                </div>

                <?/*php if (!$model->isNewRecord): ?>
                    <? echo $form->field($model, 'foto')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*', 'multiple' => false],
                        'pluginOptions' => [
                            'initialPreview' => '/' . $model->foto,
                            'initialPreviewAsData' => true,
                            'initialCaption' => '/' . $model->foto,
                            'overwriteInitial' => false,
                            'maxFileSize' => 2800
                        ]
                    ]);
                    ?>
                <? else: ?>
                    <? echo $form->field($model, 'foto')->widget(FileInput::classname(), [
                        'options' => ['accept' => 'image/*'],
                    ]);
                    ?>
                <? endif; */ ?>

                <? //= $form->field($model, 'maket_id')->textInput() ?>
                <?= $form->field($model, 'comment')->textarea(['rows' => 3]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Просмотр', '#', ['class' => 'btn btn-primary ajax-card-preview']) ?>
                    <?= Html::a('Закрыть', ['index'], ['class' => 'btn btn-danger']) ?>
                </div>

                <?php ActiveForm::end(); ?>

                <?php $form = ActiveForm::begin(['id' => 'form-web-pict', 'options' => ['enctype' => 'multipart/form-data']]); ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="col-md-6">
            <h3 id="block-maket-tmp">МАКЕТ КАРТЫ</h3>
            <div class="form-group webcamera-blocke hidden">
                <div class="card">
                    <div class="card__top">
                        <div class="card__photo">
                            <canvas class="photo"></canvas>
                            <video class="player" style="display: none"></video>
                            <div class="strip" style="display: none"></div>
                        </div>
                        <div class="card__info">
                            <div class="card__info-top">
                                <div class="card__stek-logo">
                                    <img src="/i/stek-logo-color.svg" alt="">
                                </div>
                                <div class="card__tu-logo">
                                    <img src="/i/tu-logo.svg" alt="">
                                </div>
                                <div class="card__barcode">
                                    <img src="/i/barcode.png" alt="">
                                </div>
                            </div>
                            <div class="card__info-header">
                                <strong>Студенческая электронная карта</strong><br>
                                Student identity card | <a href="http://stec.me">http://stec.me</a>
                            </div>
                            <div class="card__name-ru">
                                    Иванов
                                    Иван Иванович
                            </div>
                            <div class="card__name-en">
                               Ivanov Ivan
                            </div>
                            <div class="card__props">
                                Дата рождения: <span>11.11.1990</span>
                            </div>
                            <div class="card__props">
                                Факультет: <span>МАТФАК</span>
                            </div>
                            <div class="card__props">
                                Department: <span>MATFAK</span>
                            </div>
                        </div>
                    </div>
                    <div class="card__bottom">
                        <div class="card__flags">
                            <div class="card__flag card__flag_ru"></div>
                            <div class="card__flag card__flag_eu"></div>
                            <div class="card__flags-note">valid in EU</div>
                        </div>
                        <div class="card__valid">
                            <span>действительна до 31 октября</span>
                        </div>
                        <div class="card__years">
                            <div class="card__year">
                                <span>2018</span>
                                <img src="/i/stek-logo-bw.png" alt="">
                            </div>
                            <div class="card__year">
                                <span>2019</span>
                                <img src="/i/stek-logo-bw.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="card__univercity-name">
                        <svg id="svgRoot" class="content" preserveAspectRatio="xMidYMid meet" viewBox="0 0 50 20">
                            <text id="text" x="0" y="0" font-size="14" dy="0">
                                <tspan x="0" y="14">ПетрГУ</tspan>
                            </text>
                        </svg>
                    </div>
                </div>

                <button type="button" class="btn btn-success" onClick="takePhoto()">
                    <span class="glyphicon glyphicon-camera"></span> Сделать снимок
                </button>
                <button type="button" class="btn btn-danger foto-camera">
                    <span class="glyphicon glyphicon-eye-close"></span> Закрыть
                </button>
            </div>
            <div id="ajax-card">
            </div>
        </div>
    </div>
</div>
<script>

</script>
<?
$this->registerJs("
$('.foto-camera').click(function () {
        if( $('.webcamera-blocke').hasClass('hidden')){
            $('.webcamera-blocke').removeClass('hidden');
            $('#ajax-card .card').addClass('hidden');
                  $('body,html').animate({
        scrollTop: $('#block-maket-tmp').offset().top + 'px'
    }, 1000);
        } else {
            $('.webcamera-blocke').addClass('hidden');
 $('#ajax-card .card').removeClass('hidden');
        }
          
    });
");

$this->registerJs("
        $('.modal-add').on('click', function () {
        console.log($(this).attr('data-target'));
        $($(this).attr('data-target')).modal('show')
            .find('#modal-content')
            .load($(this).attr('data-href'));
    });

    function readURL(input) {


        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
              
  
            $(document).find('#ajax-card .card__photo img').attr('src', e.target.result);
            $(document).find('#temp-webcam').val('');
                
            };

            reader.readAsDataURL(input.files[0]);
        } else {
         if($(document).find('#temp-webcam').length > 0){
 $(document).find('#ajax-card .card__photo img').attr('src',$(document).find('#temp-webcam').val());
            }
        }
    }

    if ($('#ajax-card').length > 0) {
        previewRender();
    }
    $(document).on('change', 'form#form-students-add input, form#form-students-add select', function () {


    if( $(this).attr('max') < $(this).val() && $(this).attr('type') == 'number'){
    return false;
    }
        if ($(this).attr('id') == 'students-foto') {
            readURL(this);
            return false;
        }
        previewRender();
    });

    function previewRender() {
        console.log('previewRender');
        var data = [];
        data = $('form#form-students-add').serializeArray();
        $.ajax({
            url: '/students/preview',
            type: 'POST',
            data: {data: data},
            success: function (result) {
                if (result != '') {
                    $('#ajax-card').html(result);

                    var input =  $(document).find('input#students-foto').get(0);
                    readURL(input);                    
                }
            },
        });
    }
");
$this->registerJs("
   
    /*$('#students-department_id').change(function () {
        var id = $(this).val();
        $('#students-specialty_id').val('');
        $('#students-specialty_id option').each(function (key, value) {

            if (id != $(this).attr('data-department_id')) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    });*/
    
");
?>

<?php
yii\bootstrap\Modal::begin([
    'header' => 'Форма добавления',
    'id' => 'modal-department',
    'size' => 'modal-md',
]);
?>
<div id='modal-content'>Загружаю...</div>
<?php yii\bootstrap\Modal::end(); ?>

<?php
yii\bootstrap\Modal::begin([
    'header' => 'Форма добавления',
    'id' => 'modal-specialty',
    'size' => 'modal-md',
]);
?>
<div id='modal-content'>Загружаю...</div>
<?php yii\bootstrap\Modal::end(); ?>
