<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Students;
/*use yii\imagine\Image;
use Imagine\Image\Box;*/

/* @var $this yii\web\View */
/* @var $model app\models\Students */

$this->title = $model->name . ' ' . $model->surname;
$this->params['breadcrumbs'][] = ['label' => 'Список студентов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-list-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

<?

/*$barcode = new  \app\controllers\Barcode($model->card_id, 1);

imagepng($barcode->image(), "barcode.png");

$imagine = Image::getImagine();
$image = $imagine->open(Yii::getAlias('@webroot/'.'barcode.png'));
$image->resize(new Box(100, 50))->save(Yii::getAlias('@webroot/'.'barcode1.png', ['quality' => 100]));*/

    ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date_add',
            'date_update',
            'surname',
            'name',
            'middle_name',
            'universityName',
            'departmentName',
            'specialtyName',
            'course',
            'validity',
            'date_birth',
            'phone',
            ['attribute' => 'form_training', 'value' => $model->formtrainingName],
            ['attribute' => 'print', 'value' => $model->printStatus ],
            'card_id',
            'comment:ntext',
            [
                'attribute'=>'Фото',
                'value'=>$model->foto,
                'format' => ['image',],
            ],
            'maket_id',
        ],
    ]) ?>

</div>
