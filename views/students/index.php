<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Students;
use kartik\daterange\DateRangePicker;
use yii\helpers\Url;
use app\models\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список студентов';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="user-list-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (Yii::$app->user->identity->role == User::ROLE_SUPERADMIN) {
        echo $this->render('_search', ['model' => $searchModel]);
    }
 ?>


    <div>
        <?= Html::a('Добавить студента', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Скачать макет ', ['download'], ['class' => 'btn btn-primary ajax-maket-download']) ?>
        <?= Html::a('Удалить ', '#', ['class' => 'btn btn-danger ajax-all-delete']) ?>
        <?= Html::a('Очистить Фильтр    ', ['index'], ['class' => 'btn btn-primary']) ?>
        <a data-pjax="0" data-toggle='modal' data-target='#Settings'
           href="<?= Url::to(['site/settings', 'model' => 'students']); ?>"
           class="btn btn-info pull-right modalButton">Настройка</a>

        <?
        // Количество записей
        $values = Yii::$app->params['pagelist'];
        $current = $dataProvider->getPagination()->getPageSize();
        ?>
        <div class="dataTables_length pull-right" id="datatable_length"><label>Показывать по
                <select class="form-control" onchange="location = this.value;">
                    <?php foreach ($values as $value): ?>
                        <option value="<?= Html::encode(Url::current(['per-page' => $value, 'page' => null])) ?>"
                                <?php if ($current == $value): ?>selected="selected"<?php endif; ?>><?= $value ?></option>
                    <?php endforeach; ?>
                </select> записи </label></div>

    </div>


    <?

    //$colomnTable[] = array('class' => 'yii\grid\SerialColumn');
    $colomnTable[] = array('class' => 'yii\grid\CheckboxColumn');
    // Получаем из настроек
    $UserSettings = unserialize(Yii::$app->user->identity->settings);
    $attributeLabels = Students::attributeLabels();

    if (empty($UserSettings) && empty($UserSettings['students'])) {


        foreach ($attributeLabels as $index => $Label) {
            $UserSettings['students'][] = $index;
        }
    }


    foreach ($UserSettings['students'] as $label) {

        if (array_key_exists($label, $attributeLabels)) {
            switch ($label) {
                case 'university_id':
                case 'department_id':
                case 'specialty_id':
                    continue;
                    break;
                case 'date_update':
                case 'date_add':
                    $colomnTable[] = ['attribute' => $label, 'format'=> 'datetime', 'value' => $label, 'filter' =>
                        DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => $label,
                            //'convertFormat' => true,
                            'options' => ['placeholder' => 'Выберите дату ...', 'class' => 'form-control'],
                            'language' => 'ru',
                            'pluginOptions' => [
                                'timePicker' => false,
                                'timePickerIncrement' => 30,
                                'locale' => [
                                    'format' => 'D.MM.Y'
                                ]
                            ]
                        ])];
                    break;
                case 'print':
                    $colomnTable[] = [
                        'attribute' => $label, 'value' => 'printStatus', 'filter' => Students::getPrintStatusList(),
                    ];
                    break;
                case 'form_training':
                    $colomnTable[] = [
                        'attribute' => $label, 'value' => 'formtrainingName', 'filter' => Students::getFormTraining(),
                    ];
                    break;
                default:
                    $colomnTable[] = $label;
                    break;
            }
        }
    }

    $colomnTable[] = array(
        'class' => 'yii\grid\ActionColumn',
        'template' => '{download}{view}{update}{delete}',
        'buttons' => [
            'download' => function ($url, $model, $key) {
                return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', ['download', 'id' => $model->id] , [
                    'title' => 'Скачать макет',
                    'class' => '']);
            }

        ],
    );
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}\n{summary}",

        'columns' => $colomnTable
        /*'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'date_add:date',
            ['attribute' => 'date_add', 'format' => 'date', 'value' => 'date_add', 'filter' =>
                DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date_add',
                    //'convertFormat' => true,
                    'options' => ['placeholder' => 'Выберите дату ...', 'class' => 'form-control'],
                    'language' => 'ru',
                    'pluginOptions' => [
                        'timePicker' => false,
                        'timePickerIncrement' => 30,
                        'locale' => [
                            'format' => 'D.MM.Y'
                        ]
                    ]
                ])],
            'date_update:date',
            'surname',
            'name',
            'middle_name',
            'university.name',
            'department.name_ru',
            'specialty_id',

            //'course',
            //'validity',
            //'date_birth',
            //'phone',
            //'form_training',
            //'print',
            //'card_id',
            //'comment:ntext',
            //'foto',
            //'maket_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],*/
    ]); ?>
</div>
<?
$this->registerJs("
jQuery(document).on('submit', '#settingsform', function (e){
        e.preventDefault();
         var url = location.href;
         var idform = $(this).attr('data-target');
        $.get($(this).attr('data-href'), function(data) {
            location.href = url;
        });
    });
");
$this->registerJs("
    $('.ajax-all-delete').on('click', function(e) {
        e.preventDefault();
        result = confirm('Вы уверены, что хотите удалить выбранные элементы?');
        if(result){
        var data = [];
        $('.grid-view tbody tr').each(function(){
            if($(this).find('input[name=\"selection[]\"]').prop('checked')){
                data[data.length] = $(this).attr('data-key')
            }
        });
        console.log(data);
        $.ajax({
            url: '/students/delete-all',
            type:'POST',
            data:{data: data},

        });
        }
    });
    $('.ajax-maket-download').on('click', function(e) {
        e.preventDefault();
        result = confirm('Вы уверены, что хотите сформировать макеты для выбранные элементов?');
        if(result){
        var data = [];
        $('.grid-view tbody tr').each(function(){
            if($(this).find('input[name=\"selection[]\"]').prop('checked')){
                data[data.length] = $(this).attr('data-key')
            }
        });
        console.log(data);
        $.ajax({
            url: '/students/download-all',
            type:'POST',
            data:{data: data},
            success: function(res){
             console.log(res);
            }
        });
        }
    });
");

?>
<script>

</script>