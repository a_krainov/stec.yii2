const video = document.querySelector('.player');
const canvas = document.querySelector('.photo');
const ctx = canvas.getContext('2d');
const strip = document.querySelector('.strip');
//const snap = document.querySelector('.snap');

function getVideo() {
  navigator.mediaDevices.getUserMedia({ video: true, audio: false })
    .then(localMediaStream => {
      console.log(localMediaStream);
      video.src = window.URL.createObjectURL(localMediaStream);
      video.play();
    })
    .catch(err => {
      console.error(`OH NO!!!`, err);
    });
}

function paintToCanavas() {
  // const width = video.videoWidth;
  // const height = video.videoHeight;
  const width = '387';
  const height = '290';
   // canvas.width = width;
   // canvas.height = height;
  canvas.width = '387';
  canvas.height = '290';

  return setInterval(() => {
    ctx.drawImage(video, 0, 0, width, height);
    // take the pixels out
    let pixels = ctx.getImageData(0, 0, width, height);
    // mess with them
    // pixels = redEffect(pixels);

    //pixels = rgbSplit(pixels);
    // ctx.globalAlpha = 0.8;

    // pixels = greenScreen(pixels);
    // put them back
    ctx.putImageData(pixels, 0, 0);
  }, 16);
}

function takePhoto() {
  // played the sound
  //snap.currentTime = 0;
  //snap.play();

  // take the data out of the canvas
  const data = canvas.toDataURL('image/jpeg');

  const link = document.createElement('a');
  link.href = data;
  link.setAttribute('download', 'handsome');
  link.innerHTML = `<img src="${data}" class="web-preview-img" alt="Handsome Man" />`;
  //strip.insertBefore(link, strip.firsChild);
  strip.innerHTML = '';
  strip.insertBefore(link, strip.firsChild);

  // Пробуем добавить картинку
    var dataURI = data;
    var blob = dataURItoBlob(dataURI);

// you can even pass a <form> in this constructor to add other fields


    var formData = new FormData(document.getElementById('form-web-pict'));
/*    var input=document.createElement('input');
    input.type="file";*/
var namefile = $('#form-web-pict [name="_csrf"]').val().slice(0,30)+'.jpg';

    formData.append('webpict', blob, namefile);



    var xhr = new XMLHttpRequest();
    xhr.open('post', '/students/webcam', true);
    xhr.send(formData)

    xhr.onreadystatechange = function() { // (3)
        if (xhr.readyState != 4) return;
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            if($('#temp-webcam').length > 0){
                $('#ajax-card .card').removeClass('hidden');
                $(document).find('#temp-webcam').val(xhr.responseText);
                $(document).find('#ajax-card .card__photo img').attr('src', data);
            } else {
                $('#form-students-add').append('<input type="hidden" id="temp-webcam" name="Students[webcam]" value="'+xhr.responseText+'">');
                $(document).find('#ajax-card .card__photo img').attr('src', data);
            }
            $(document).find('#students-foto').val('');
        }

    }
}


function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var arrayBuffer = new ArrayBuffer(byteString.length);
    var _ia = new Uint8Array(arrayBuffer);
    for (var i = 0; i < byteString.length; i++) {
        _ia[i] = byteString.charCodeAt(i);
    }

    var dataView = new DataView(arrayBuffer);
    var blob = new Blob([dataView], { type: mimeString });
    return blob;
}
function redEffect(pixels) {
  for(let i = 0; i < pixels.data.length; i+=4) {
    pixels.data[i + 0] = pixels.data[i + 0] + 200; // RED
    pixels.data[i + 1] = pixels.data[i + 1] - 50; // GREEN
    pixels.data[i + 2] = pixels.data[i + 2] * 0.5; // Blue
  }
  return pixels;
}

function rgbSplit(pixels) {
  for(let i = 0; i < pixels.data.length; i+=4) {
    pixels.data[i - 150] = pixels.data[i + 0]; // RED
    pixels.data[i + 500] = pixels.data[i + 1]; // GREEN
    pixels.data[i - 550] = pixels.data[i + 2]; // Blue
  }
  return pixels;
}

function greenScreen(pixels) {
  const levels = {};

  document.querySelectorAll('.rgb input').forEach((input) => {
    levels[input.name] = input.value;
  });

  for (i = 0; i < pixels.data.length; i = i + 4) {
    red = pixels.data[i + 0];
    green = pixels.data[i + 1];
    blue = pixels.data[i + 2];
    alpha = pixels.data[i + 3];

    if (red >= levels.rmin
      && green >= levels.gmin
      && blue >= levels.bmin
      && red <= levels.rmax
      && green <= levels.gmax
      && blue <= levels.bmax) {
      // take it out!
      pixels.data[i + 3] = 0;
    }
  }

  return pixels;
}

getVideo();

video.addEventListener('canplay', paintToCanavas);
