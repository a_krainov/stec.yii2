<?php

namespace app\controllers;

use Yii;
use app\models\SpecialtyList;
use app\models\SpecialtyListSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;

/**
 * SpecialtyController implements the CRUD actions for SpecialtyList model.
 */
class SpecialtyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'index', 'create', 'view', 'update'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SpecialtyList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SpecialtyListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        //если юсер не суперадмин, то только свои ВУЗ
        /*if (Yii::$app->user->identity->role == User::ROLE_ADMIN){
            $dataProvider->query->andWhere(['university_id' => Yii::$app->user->identity->university_id]);
        }*/

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SpecialtyList model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        //если юсер не суперадмин, то только свои ВУЗ
        if ($model->university_id != Yii::$app->user->identity->university_id && Yii::$app->user->identity->role == User::ROLE_ADMIN){
            throw new ForbiddenHttpException('В доступе отказано.');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SpecialtyList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SpecialtyList();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SpecialtyList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //если юсер не суперадмин, то только свои ВУЗ
        if ($model->university_id != Yii::$app->user->identity->university_id && Yii::$app->user->identity->role == User::ROLE_ADMIN){
            throw new ForbiddenHttpException('В доступе отказано.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SpecialtyList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SpecialtyList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SpecialtyList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SpecialtyList::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAddAjax()
    {
        $model = new SpecialtyList();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->layout = false;
            return $this->render('confirm');
        }
        return $this->renderAjax('create-ajax', ['model' => $model]);
    }
}
