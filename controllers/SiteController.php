<?php

namespace app\controllers;

use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\Controller;
use app\models\Students;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isPost) {

            return $this->render('index', [
                'searchModel' => Yii::$app->request->post()
            ]);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password_hash = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionForgotpassword()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post()) && $model->login != '') {


            $user = User::findOne(['login' => $model->login]);

            if ($user) {
                if ($user->status == User::STATUS_ACTIVE) {

                    $newpassword = (string)rand(100001, 999999);

                    $user->password_hash = $newpassword;

                    $user->save('test');

                    //Yii::$app->session->setFlash('success', $newpassword);

                    if ($user->email != '') {
                        $strText = Yii::$app->params['settings']['textMailUserPassword'];
                        $strText = str_replace("#NAME#", $user->name, $strText);
                        $strText = str_replace("#LOGIN#", $user->login, $strText);
                        $strText = str_replace("#PASSWORD#", $newpassword, $strText);
                        $strText = str_replace("#URL_SITE#", $_SERVER['SERVER_NAME'], $strText);
                        $strText = str_replace("#adminEmail#", Yii::$app->params['adminEmail'], $strText);

                        $arResult = $strText;

                        Yii::$app->mailer->compose('reset_password', ['arResult' => $arResult])
                            ->setFrom(Yii::$app->params['sendFrom'])
                            ->setTo($user->email)
                            ->setSubject('Восстановление пароля ' . $_SERVER['SERVER_NAME'])
                            ->send();
                    }


                    Yii::$app->session->setFlash('forgotpasswordSubmitted');

                } else {
                    Yii::$app->session->setFlash('error', Yii::$app->params['settings']['errorNoActiveUserPassword'] . ' ' . Yii::$app->params['adminEmail']);
                }
            } else {

                Yii::$app->session->setFlash('error', Yii::$app->params['settings']['errorNotUserPassword'] . ' ' . Yii::$app->params['adminEmail']);
            }

        }

        return $this->render('forgotpassword', [
            'model' => $model,
        ]);
    }

    // настройка для таблиц

    public function actionSettings($model)
    {
        $this->layout = false;

        $model = \Yii::$app->request->get('model');
        $result = false;
        if(Yii::$app->request->post()){

            // Добаляем пользователю сериалайз массив
            $UserSettings[$model] = Yii::$app->request->post('settingsForm');
            $UserSettings = serialize($UserSettings);

            $user = User::findOne(['id' => Yii::$app->user->identity->id]);
            $user->settings = $UserSettings;
            $user->save();
            $result = true;
        }
        // Если в настройках у пользователя нет данных
        /*
         * Пример массива
         * array(
            'modelName1' => array('0' => 'colomnName1','1' => 'colomnName2'),
            'modelName2' => array('0' => 'colomnName2','1' => 'colomnName1')
        )
         * */

        $UserSettings = unserialize(Yii::$app->user->identity->settings);

        if (empty($UserSettings)) {
            $UserModelSettings = null;
        } else {
            if(!array_key_exists($model, $UserSettings)){
                $UserModelSettings = null;
            } else {
                $UserModelSettings = $UserSettings[$model];
            }
        }

        // Получаем колонки модели
        switch ($model){
            case 'students':
                $attributeLabels = Students::attributeLabels();
                break;
        }

        if (empty($UserModelSettings)) {
           foreach ($attributeLabels as $index => $Label){
               $UserModelSettings[] = $index;
           }
        }

        // Добавляем если добавились новые колонки для пользователя
        // Рендерим форму для выбора для пользователя





        return $this->renderAjax('settings', compact('UserModelSettings', 'attributeLabels', 'result'));
    }
}
