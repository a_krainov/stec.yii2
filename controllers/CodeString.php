<?php
/**
 * Created by PhpStorm.
 * User: Эдуард
 * Date: 24.05.2018
 * Time: 11:09
 */

namespace app\controllers;


use Picqer\Barcode\BarcodeGenerator;

class CodeString extends BarcodeGenerator
{
    public function getInt($code, $type)
    {
        $barcodeData = $this->getBarcodeData($code, $type);

        return $barcodeData['code'];
    }
}