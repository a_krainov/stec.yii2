<?php

namespace app\controllers;

use app\models\DepartmentList;
use app\models\MaketList;
use app\models\SpecialtyList;
use app\models\UniversityList;
use Yii;
use app\models\Students;
use app\models\StudentsSearch;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;
use \yii\db\Query;
use yii\web\UploadedFile;
use app\models\ExportPdf;
use ZipArchive;


/**
 * StudentsController implements the CRUD actions for Students model.
 */
class StudentsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'index', 'create', 'view', 'update', 'preview', 'print'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'create', 'view', 'update', 'preview', 'print'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Finds the Students model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Students the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Students::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function createZip($files)
    {
        $fileArhName = '/uploads/tmp/' . 'maket_archive_' . time() . '.zip';

        $zip = new \ZipArchive();

        if ($zip->open(Yii::getAlias('@webroot') . $fileArhName, ZipArchive::CREATE) !== TRUE) {
            throw new \Exception('Cannot create a zip file');
        }

        foreach ($files as $file) {
            $filename = explode('/', $file);
            $zip->addFile($file, end($filename));
        }

        $zip->close();
        return $fileArhName;
    }

    /**
     * Lists all Students models.
     * @return mixed
     */
    public function actionIndex()
    {
        // Форма смена университета для пользователя
        if (Yii::$app->request->isPost) {
            $dataPost = Yii::$app->request->post();
            if ($dataPost['StudentsSearch']['university_id'] != '' && $dataPost['change_university_id'] == 'Y') {
                $user = User::findOne(Yii::$app->user->identity->id);
                $user->university_id = $dataPost['StudentsSearch']['university_id'];
                $user->save();
                return $this->redirect('index');
            }
        }


        $searchModel = new StudentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Students model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionWebcam()
    {
        if (Yii::$app->request->isPost) {


            $uploaddir = Yii::$app->basePath . '/web';
            $uploadname = '/uploads/tmp/' . basename($_FILES['webpict']['name']);
            $uploadfile = $uploaddir . $uploadname;

            if (move_uploaded_file($_FILES['webpict']['tmp_name'], $uploadfile)) {
                return $uploadname;
            } else {
                return "error";
            }

        }
    }

    /**
     * Creates a new Students model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Students();

        if (Yii::$app->request->isPost) {


            $dataPost = Yii::$app->request->post();

            // Проверяем есть ли картинка с веб камеры
            if (isset($dataPost['Students']['webcam']) && $dataPost['Students']['webcam'] != '') {

                $filename = str_replace('/uploads/tmp/', "", $dataPost['Students']['webcam']);
                if (!is_dir('uploads/university/' . $dataPost['Students']['university_id'] . '/')) {
                    mkdir('uploads/university/' . $dataPost['Students']['university_id'] . '/', 0755);
                }
                rename(Yii::getAlias('@webroot') . $dataPost['Students']['webcam'], Yii::getAlias('@webroot') . '/uploads/university/' . $dataPost['Students']['university_id'] . '/' . $filename);
                $webcamPath = 'uploads/university/' . $dataPost['Students']['university_id'] . '/' . $filename;


                $model->load(Yii::$app->request->post());
                $model->foto = $webcamPath;
                if ($model->save()) {


                    // Генерируем макет
                    $this->actionPrint($model->id);

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }


            $model->foto = UploadedFile::getInstance($model, 'foto');

            if ($model->upload()) {
                $path = $model->foto;

                $model->load(Yii::$app->request->post());
                $model->foto = $path;

                if ($model->save()) {


                    // Генерируем макет
                    $this->actionPrint($model->id);

                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionPreview()
    {
        $this->layout = false;
        $modelPost = Yii::$app->request->post();
        $model = $modelPost['data'];

        return $this->renderAjax('preview', compact('model'));

        /*        return $this->render('preview', [
                    'model' => $model,
                ]);*/
    }

    /**
     * Updates an existing Students model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {

            $arrfoto = UploadedFile::getInstance($model, 'foto');

            if (!empty($arrfoto)) {
                $model->foto = $arrfoto;
                if ($model->upload()) {
                    $path = $model->foto;

                    $model->load(Yii::$app->request->post());
                    $model->foto = $path;

                    if ($model->save()) {

                        // Генерируем макет
                        $this->actionPrint($model->id);

                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            } else {
                $model->load(Yii::$app->request->post());

                if ($model->save()) {

                    // Генерируем макет
                    $this->actionPrint($model->id);
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }


        }

        /*        if ($model->load(Yii::$app->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }*/

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Students model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        // Сначала удаляем Макет
        if ($model->maket_id != '') {
            $maket = MaketList::findOne($model->maket_id);
            FileHelper::unlink(Yii::getAlias('@webroot/') . $maket->path);
        }

        if ($model->foto != '') {
            // Потому удаляем картинку
            FileHelper::unlink(Yii::getAlias('@webroot/') . $model->foto);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteAll()
    {

        if (Yii::$app->request->post()) {
            $postData = Yii::$app->request->post();
            foreach ($postData['data'] as $idItem) {
                $this->actionDelete($idItem);
            }
            Yii::$app->session->setFlash('success', 'Записи удалены.');

            return $this->redirect(['index']);
        }

    }

    public function actionDownloadAll()
    {
        //$start = microtime(true);

        if (Yii::$app->request->post()) {
            $postData = Yii::$app->request->post();


            $allPath = array();

            foreach ($postData['data'] as $idItem) {
                $model = Students::findOne($idItem);
                // Проверяем создал ли макет
                if ($model->maket_id == null) {
                    $filePath = $this->actionPrint($idItem)->path;
                } else {
                    $filePath = MaketList::findOne($model->maket_id)->path;
                }
                $allPath[] = Yii::getAlias('@webroot/') . $filePath;
                if($model->print == 0){
                    // Если у стунеднат не было флага Печать, тогда ставим
                    $model->print = 1;
                    $model->save();
                }
            }

            $linkArh = $this->createZip($allPath);

            Yii::$app->session->setFlash('success', 'Файлы сформированы в архив <a href="' . $linkArh . '" download>' . $linkArh . '</a>');

            return $this->redirect(['index']);
            //return round(microtime(true) - $start, 4);
        }

    }


    public function actionDownload($id)
    {

        // ВРЕМЕННЫЙ КОД
        $model = $this->findModel($id);

        $this->layout = false;

        $maket = $this->actionPrint($model->id);

        return $this->redirect('/' . $maket->path);
        // ВРЕМЕННЫЙ КОД

        if ($model->maket_id != '') {
            $maket = MaketList::findOne($model->maket_id);

            return $this->redirect('/' . $maket->path);
        } else {
            $this->layout = false;

            $maket = $this->actionPrint($model->id);

            return $this->redirect('/' . $maket->path);
        }
    }

    /**
     * Вывод карточки в pdf
     */
    public function actionPrint($id)
    {
        $this->layout = false;

        $data = Students::find()->where(['id' => $id])->asArray()->one();


        //$pdf = new Pdf($_SERVER['DOCUMENT_ROOT'] . '/web/page.html');

        $content = $this->renderPartial('_templ_maketcard', [
            'model' => $data,
        ]);

        /*        $mpdf = new \Mpdf\Mpdf();
                $mpdf->WriteHTML($content);
                $mpdf->Output();

                die();
                return $this->renderPartial('_templ_maketcard', [
                    'model' => $data,
                ]);*/


        $path = ExportPdf::save($content, $data);

        // Добавил путь в таблицу и обновим для элемента
        $makreList = new MaketList;
        $makreList->path = $path;
        $makreList->save();

        // Обновим данные
        $elementUpdate = Students::findOne($id);

        if($elementUpdate->maket_id != NULL){
            // Запоминаем ID макета
            $delMaket = $elementUpdate->maket_id;

            // Указываем стунеднту новую макет
            $elementUpdate->maket_id = $makreList->id;
            $elementUpdate->save();

            // Удаляем из базы старый
            $maketData = MaketList::findOne($delMaket);
            $maketData->delete();

        } else {
            $elementUpdate->maket_id = $makreList->id;
            $elementUpdate->save();
        }

        return $makreList;
        //return $this->redirect(['view', 'id' => $id]);

    }

    /*************
     * Controller ajax Select2
     ************/
    public function actionDepartmentAjax($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];


        if (!is_null($q)) {
            $query = new Query;

            $query->select('id, name_ru AS text')
                ->from('department_list')
                ->where(['like', 'name_ru', $q])
                ->andWhere(['=', 'university_id', Yii::$app->user->identity->university_id])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => DepartmentList::find($id)->name_ru];
        } else {
            $out['results'] = DepartmentList::find()->select('id,name_ru as text')->where(['university_id' => Yii::$app->user->identity->university_id])->asArray()->all();
        }

        return $out;
    }

    public function actionSpecialtyAjax($q = null, $id = null, $department = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q) && !is_null($department)) {
            $query = new Query;

            $query->select('id, name AS text')
                ->from('specialty_list')
                ->where(['like', 'name', $q])
                ->andWhere(['=', 'university_id', Yii::$app->user->identity->university_id])
                ->andWhere(['=', 'department_id', $department])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif (!is_null($department)) {
            $out['results'] = SpecialtyList::find()->select('id,name as text')->where(['department_id' => $department])->andWhere(['university_id' => Yii::$app->user->identity->university_id])->asArray()->all();
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => SpecialtyList::find($id)->name];
        }
        return $out;
    }

    public function actionUniversityAjax($q = null, $id = null)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];


        if (!is_null($q)) {
            $query = new Query;

            $query->select('id, name AS text')
                ->from('university_list')
                ->where(['like', 'name', $q])
                ->limit(20);

            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        } elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => UniversityList::find($id)->name];
        } else {
            $out['results'] = UniversityList::find()->select('id,name as text')->asArray()->all();
        }

        return $out;
    }

}
